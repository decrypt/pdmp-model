#!/usr/bin/env python3
# -*- coding: utf-8 -*-


###############################################################################
#### VISUALISATION ########
###########################


import os
import copy
import warnings
import pickle
import itertools
import functools

import numpy as np
import numba
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors


import parameters as param
import parameters_chem as param_c

pop_hd = param.pop_hd
jmp_hd = param.jmp_hd


###############################################################################
# Color and naming convention throughout graph

# mpl.rc('text', usetex=True)  # to use Tex fonts in matplotlib
mpl.rc('font', family='serif')
small = 11  # standard latex font size
medium = 12
big = 15
vbig = 17
mpl.rc('figure', figsize=(6.2, 4.65))  # standard latex avec marge 1 inch
mpl.rc('axes', titlesize=big)
mpl.rc('axes', labelsize=big)  # medium)
mpl.rc('xtick', labelsize=big)
mpl.rc('ytick', labelsize=big)
mpl.rc('legend', fontsize=big)
mpl.rc({"axes.grid": True, "grid.zorder": 0})


# Color
blue = 'cornflowerblue'
red = 'tab:red'
green = 'tab:green'

# Set color code for each kind of cell and jump, and a dictionnary which give the full name
# associated to keys (useful for title of graphs,etc).

color_code = {}
cells_name = {}
if 'sc' in param.cell_types:
    color_code['sc'] = blue
    cells_name['sc'] = 'stem'

if 'pc' in param.cell_types:
    color_code['pc'] = red
    cells_name['pc'] = 'progenitor'

if 'gc' in param.cell_types:
    color_code['gc'] = 'slateblue'
    cells_name['gc'] = 'gobelet'

if 'dcs' in param.cell_types:
    color_code['dcs'] = 'gray'
    cells_name['dcs'] = 'DCS'

if 'ent' in param.cell_types:
    color_code['ent'] = green
    cells_name['ent'] = 'enterocyte'


if 'ex' in param.jump_types:
    color_code['ex'] = 'black'

if 'buty' in param_c.chem:
    color_code['buty'] = 'coral'

if 'o2' in param_c.chem:
    color_code['o2'] = 'seagreen'


###############################################################################
# Initialization


def load_multiple_simu(folder):

    files = os.listdir(folder)

    # Load results of the simulation
    files_npz = [x for x in files if ".npz" in x]

    result = (np.load(folder + '/' + x) for x in files_npz)

    return result


def load_one_simu(simu_path):
    result = np.load(simu_path)
    return result


def set_xtick(tick, label):
    fig = plt.gcf()
    ax = plt.gca()

    l = [w.get_text() for w in ax.get_xticklabels()]
    t = list(ax.get_xticks())

    l += label
    t += tick
    ax.set_xticks(t)
    ax.set_xticklabels(l)


def set_ytick(tick, label):
    fig = plt.gcf()
    ax = plt.gca()

    l = [w.get_text() for w in ax.get_yticklabels()]
    t = list(ax.get_yticks())

    l += label
    t += tick
    ax.set_yticks(t)
    ax.set_yticklabels(l)


def save(name, dpi=300):
    name_png = name + '.png'
    name_svg = name + '.eps'
    plt.savefig(name_png, format='png', dpi=dpi)
    plt.savefig(name_svg, format='eps')

    # pickle.dump(plt.gcf(), open(f'{name}.pickle', 'wb'))


def list_array(tup):
    import copy
    k = len(tup)-1
    out = []
    while k >= 0:
        out = [copy.deepcopy(out) for i in range(tup[k])]
        k += -1

    return out


def color_scale(colora, colorb, n):

    color_0 = mcolors.to_rgba(colora)
    color_1 = mcolors.to_rgba(colorb)
    x0 = np.asarray(color_0)
    x1 = np.asarray(color_1)
    coeff = np.linspace(0, 1, n)
    colors_list = []
    for i in range(n):
        colors_list.append(tuple(coeff[i]*x1 + (1-coeff[i])*x0))

    return colors_list


def slice_res(result, Tstart=None, Tend=None):
    """Extract from a simulation result only the results between Tstart and Tend.

    Args:
        result (Npz object): simulation result.
        Tstart (float, optional): Start time. If None, equals start time of the original result object. Defaults to None.
        Tend (float, optional): End time. If None, equals end time of the original result object. Defaults to None.

    Returns:
        result (dictionnary): same keys (and values type) as the original result object, so it can used by other functions in the same way as the original result object.
    """

    keys_pop = np.cumsum(result['sizepop'])

    if Tstart is None:
        Tstart = result['time'][0]
        idx_s = 0
    else:
        idx_s = np.where(result['time'] >= Tstart)[0][0]
    if Tend is None:
        Tend = result['time'][-1]
        idx_e = -1
    else:
        idx_e = np.where(result['time'] >= Tend)[0][0]

    Res = {}
    Res['time'] = result['time'][idx_s:idx_e]
    if idx_s != 0:
        sizepop_delta = result['sizepop'][idx_s - 1]
    else:
        sizepop_delta = 0
    Res['sizepop'] = result['sizepop'][idx_s:idx_e] - sizepop_delta

    if idx_s == 0:
        Res['pop'] = result['pop'][:keys_pop[idx_e - 1], :]
    else:
        Res['pop'] = result['pop'][keys_pop[idx_s-1]:keys_pop[idx_e - 1], :]

    try:  # as there might be no jumps after t
        idx_js = np.where(result['jump'][:, jmp_hd['t']] >= Tstart)[0][0]

    except IndexError:  # no jumps
        message = f'No jumps after Tstart={Tstart}'
        warnings.warn(message)
        # important to keep correct shape
        Res['jump'] = np.array([[None]*len(param.jump_variables)])

    else:
        try:
            idx_je = np.where(result['jump'][:, jmp_hd['t']] >= Tend)[0][0]
        except IndexError:
            idx_je = -1
            Res['jump'] = result['jump'][idx_js:idx_je, :]

        else:
            if idx_js == idx_je:
                message = f'No jumps between Tstart={Tstart} and Tend={Tend}'
                warnings.warn(message)
                # important to keep correct shape
                Res['jump'] = np.array([[None]*len(param.jump_variables)])

            else:
                Res['jump'] = result['jump'][idx_js:idx_je, :]

    return Res


def treat_multiple_simulations(func):
    @functools.wraps(func)
    def iterated_func(result, *args, **kwargs):
        if hasattr(result, '__next__'):
            # test if result is an iterator
            Data = []
            for res in result:
                Data.append(func(res, *args, **kwargs))
            return Data
        else:
            return func(result, *args, **kwargs)

    return iterated_func

###############################################################################


def jump_diagram(result, keys_pop, type_c=None):

    pop_hd = param.pop_hd  # headers for result['pop']
    jmp_hd = param.jmp_hd  # headers for result['jump']
    jump_types = param.jump_types

    # Extract data
    jumps = result['jump']

    fig, axe = plt.subplots(1, 1)

    for typ in jump_types:
        if type_c is None:
            mask = jumps[:, jmp_hd['type_jump']] == param.jump_to_int[typ]
        else:  # select for a specific cell type
            mask = (jumps[:, jmp_hd['type_jump']] == param.jump_to_int[typ]) & (
                jumps[:, jmp_hd['type_mom']] == param.cell_to_int[type_c])

        axe.scatter(jumps[mask, jmp_hd['t']], jumps[mask,
                    jmp_hd['pos_mom']], color=color_code[typ])

    return fig


#############################################################################################

def descendance(result, cell_id, size_descendance=5):
    """Find all the cells (or the n=size_descendance first) which are part of the progeny of a mother cell given by cell_id.

    Args:
        result (Npz object): simulation result.
        cell_id (int): id of cell to take as the mother cell.
        size_descendance (int): function will return when the first n=size_descendance cells are found.

    Returns:
        cells (list): list of id the cells which are progeny of the mother cell.
    """
    id_last_cell = int(np.amax(result['pop']))
    if cell_id > id_last_cell:
        warnings.warn(
            f'cell_id values are greater than last cell {id_last_cell}')

    pop_hd = param.pop_hd
    jmp_hd = param.jmp_hd
    ex = param.jump_to_int['ex']

    cells = [cell_id]
    cells_alive = [cell_id]

    # find birth index
    if cell_id <= result['sizepop'][0] - 1:
        birth = 0
    else:
        # There seems to be no particularly fast way to find the first element of an array (numba ?but used once...)
        birth = np.where(result['jump'][:, jmp_hd['id_daug']] == cell_id)[0][0]

    n_jump = result['jump'].shape[0]
    n = birth + 1
    while (n < n_jump) & (len(cells_alive) > 0) & (len(cells) <= size_descendance):
        if result['jump'][n, jmp_hd['id_mom']] in cells_alive:
            if not np.isnan(result['jump'][n, jmp_hd['id_daug']]):  # division
                cells.append(result['jump'][n, jmp_hd['id_daug']])
                cells_alive.append(result['jump'][n, jmp_hd['id_daug']])

            elif result['jump'][n, jmp_hd['type_jump']] == ex:  # mort
                cells_alive.remove(result['jump'][n, jmp_hd['id_mom']])

        n += 1

    return cells


def find_cell_with_progeny_of_size(result, start_id, size):
    """Return a list of cells_id which are the first size members of the progeny of the first cell having equal or more
    than size cells in its progeny, starting from cell with cell_id = start_id.

    Args:
        result (Npz object): simulation result.
        start_id (int): starting id of cells to test for progeny size.
        size (int): size of progeny to test for.

    Returns:
        cells (list (size + 1)): list of cell_id. First element is the mother cell, and the rest of the element are
        the first member of the progeny of the mother cell.
    """
    id = start_id
    id_max = int(np.amax(result['pop'][:, pop_hd['id']]))
    cells = descendance(result, id, size)
    while (len(cells) <= size) and (id < id_max):
        id += 1
        cells = descendance(result, id, size)

    if len(cells) > size:
        cells = cells[:size]

    return cells

 ###################################################################################################


def f_celllife(cells_id, result):
    """Extract the spatial trajectory of cell cells_id (in 1 D) and the stochastic
    events that accured along the way, until its death or the end of the
    simulation. Can be plotted with function pltcelllife.

    ***Note***: This function can be slow if the simulations are long and/or the size of cells_id is big.

    Args:
        cells_id (list (or iterable) of int): the list of cell id to use.
        result (Npz file): result of simulation.

    Raises:
        ValueError: if an id in cells_id is above the biggest id in the simulation (and therefore does not correspond to any cell
        of the simulation).

    Returns:
        dct (dict): each attribute of dct corresponds to one aspect of the cell life (spatial trajectory, stochastic event,...).
        Each value of dct is a nested list of size len(cells_id). Element i of these nested lists correspond to the given dct attribute for the i-th
        cell in cells_id.
    """

    keys_pop = np.cumsum(result['sizepop'])

    pop_hd = param.pop_hd  # headers for result['pop']
    jmp_hd = param.jmp_hd  # headers for result['jump']

    # show the trajectory of a cell as well as the division and specialization jumps
    number_younger_cell = np.nanmax(result['pop'][:, pop_hd['id']])

    ########################################
    # Construct data
    l_positions = []
    l_times = []
    l_birth = []
    l_kind_at_birth = []
    l_position_at_birth = []
    l_pos_at_jump = []
    l_t_jump = []
    l_color_cell = []
    l_c_daughter = []

    tmin = result['time'][0]

    for cell_id in cells_id:

        if cell_id > number_younger_cell:  # Error
            raise ValueError(
                'Cell_number must be below or equal to ' + str(number_younger_cell))

        else:
            # Position: we will reconstruct position from result['pop'] and result['jump']
            index_of_cell = np.where(
                result['pop'][:, pop_hd['id']] == cell_id)[0]
            cell_position = result['pop'][index_of_cell,
                                          pop_hd['pos']]  # return position of the cell
            # cells_id over tim
            times = np.empty(index_of_cell.size)
            # as we only save snap of population, index_of_cell might be empty
            if cell_position.shape[0] != 0:
                first_time_index = np.argmax(keys_pop > index_of_cell[0])
                last_time_index = first_time_index + len(index_of_cell) - 1
                times = result['time'][first_time_index:last_time_index + 1]

            times_supp = []  # these 2 lists will store supplementary data on position at different time given by result['jump']
            pos_supp = []

            # Birth
            n0 = result['sizepop'][0]

            # cell was there at initialisation,
            if cell_id in result['pop'][:n0, pop_hd['id']]:
                # -1 as id_cell is starting at 0
                t_birth = tmin
                kind = result['pop'][index_of_cell[0], pop_hd['type_c']]
                pos_birth = result['pop'][index_of_cell[0], pop_hd['pos']]

            else:  # cell was born during simulation
                birth = result['jump'][:][result['jump']
                                          [:, jmp_hd['id_daug']] == cell_id]
                t_birth = birth[0, jmp_hd['t']]
                kind = birth[0, jmp_hd['type_jump']]
                pos_birth = birth[0, jmp_hd['pos_daug']]

            l_birth.append(t_birth)
            l_kind_at_birth.append(param.int_to_cell[kind])
            l_position_at_birth.append(pos_birth)

            times_supp += [t_birth]  # to keep list flat
            pos_supp += [pos_birth]

            # Other event
            # Time
            cell_jumps = result['jump'][:][result['jump']
                                           [:, jmp_hd['id_mom']] == cell_id]
            l_t_jump.append(cell_jumps[:, jmp_hd['t']])  # time of the jumps

            # Position
            l_pos_at_jump.append(cell_jumps[:, jmp_hd['pos_mom']])

            times_supp += list(cell_jumps[:, jmp_hd['t']])
            pos_supp += list(cell_jumps[:, jmp_hd['pos_mom']])

            # Construct color list directly
            color_cell = []  # the goal is to construct a list of color to match symbols
            # with the type of jump
            color_daughter_cells = []
            n_jumps = len(l_t_jump[-1])
            for i in range(n_jumps):
                # kind of the cell at the jump
                cell_kind = param.int_to_cell[cell_jumps[i,
                                                         jmp_hd['type_mom']]]
                color_cell.append(color_code[cell_kind])

                if cell_jumps[i, jmp_hd['type_jump']] == param.jump_to_int['ex']:  # extrusion
                    color_daughter = color_code['ex']

                elif cell_jumps[i, jmp_hd['type_jump']] == cell_jumps[i, jmp_hd['type_mom']]:  # division
                    color_daughter = color_code[param.int_to_jump[cell_jumps[i, jmp_hd['type_mom']]]]

                else:  # differenciation
                    color_daughter = color_code[param.int_to_jump[cell_jumps[i, jmp_hd['type_jump']]]]

                color_daughter_cells.append(color_daughter)

            l_color_cell.append(color_cell)
            l_c_daughter.append(color_daughter_cells)

            # Integrate times_supp and pos_supp to times and cell_position
            t_full = np.concatenate((times, times_supp))
            pos_full = np.concatenate((cell_position, pos_supp))
            order = np.argsort(t_full)
            t_final = t_full[order]
            pos_final = pos_full[order]

            l_positions.append(pos_final)
            l_times.append(t_final)

    dct = {}
    dct['pos'] = l_positions
    dct['times'] = l_times
    dct['birth'] = l_birth
    dct['kind_b'] = l_kind_at_birth
    dct['pos_b'] = l_position_at_birth
    dct['pos_j'] = l_pos_at_jump
    dct['t_j'] = l_t_jump
    dct['col_c'] = l_color_cell
    dct['col_d'] = l_c_daughter

    return dct


def plot_celllife(cells_id, result, msize=12):
    """Plot the life of cells with id in cells_id.

    ***Note***: this function can be slow if the simulations are long and/or the size of cells_id is big.

    Args:
        cells_id (list (or iterable) of int): the list of cell id to plot.
        result (Npz file): result of simulation.
        msize (int, optional): Size of markers in plot. Defaults to 12.

    Returns:
        fig (Matplotlib figure).
    """

    ########################################################
    # Data
    dic_celllife = f_celllife(cells_id, result)

    l_positions = dic_celllife['pos']
    l_times = dic_celllife['times']
    l_birth = dic_celllife['birth']
    l_kind_at_birth = dic_celllife['kind_b']
    l_position_at_birth = dic_celllife['pos_b']
    l_pos_at_jump = dic_celllife['pos_j']
    l_t_jump = dic_celllife['t_j']
    l_color_cell = dic_celllife['col_c']
    l_c_daughter = dic_celllife['col_d']

    ##########################################################
    # Plotting
    fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(8.0, 4.65))
    ax.grid(b=True, zorder=0)

    # Ugly: must plot each point one by one to vary shape and filling of markers...
    for i, cell_id in enumerate(cells_id):
        # label = 'cell '+ str(cell_id), lw =1)
        plt.plot(l_times[i], l_positions[i], color='k', lw=1)

        # Plot birth
        colors_birth = [color_code[i] for i in l_kind_at_birth]
        plt.plot(l_birth[i], l_position_at_birth[i], marker='o', color=colors_birth[i],
                 markersize=msize, markeredgecolor='none')

    # Plot events: even uglier, we control the type of events through the type of colors passed. If two events have the same colors,
    # the code will output something wrong

        for k in range(len(l_t_jump[i])):

            # check for division event (very ugly)
            if l_color_cell[i][k] == l_c_daughter[i][k]:

                plt.plot(l_t_jump[i][k], l_pos_at_jump[i][k], marker='P', c=l_color_cell[i][k], markersize=msize,
                         markeredgecolor='none')

            # Check for extrusion
            elif l_c_daughter[i][k] == color_code['ex']:
                plt.plot(l_t_jump[i][k], l_pos_at_jump[i][k], marker='X', c=l_c_daughter[i][k], markersize=msize,
                         markeredgecolor='none')

            # Differenciation
            else:

                plt.plot(l_t_jump[i][k], l_pos_at_jump[i][k], marker='D', fillstyle='left', c=l_color_cell[i][k],
                         markerfacecoloralt=l_c_daughter[i][k], markersize=msize,
                         markeredgecolor='none')

    #######################################################################
    # Set up the rest of the figure
    # Axis
    plt.ylim(0-4, param.z_max+4)
    t_min, t_max, foo, foo = plt.axis()
    plt.plot([t_min, t_max], 0*np.ones(2), '--', [t_min, t_max],
             param.z_max*np.ones(2), '--', c='grey', lw=1.5)

    # Legend independantly
    plt.plot([], [], color='w', marker='o', fillstyle='none',
             markeredgecolor='k', label='birth', markersize=msize)
    plt.plot([], [], color='w', marker='P', fillstyle='none',
             markeredgecolor='k', label='division', markersize=msize)
    plt.plot([], [], color='w', marker='D', fillstyle='none',
             markeredgecolor='k', label='differentiation', markersize=msize - 3)
    for i in param.cell_types:  # loop over the kind of cell
        plt.plot([], [], 's', color=color_code[i], label=cells_name[i])
    plt.plot([], [], color=color_code['ex'],
             marker='X', label="extrusion", markersize=msize)
    plt.legend(loc=0)

    # Axis title
    plt.gca().set_xlabel('time ($h$)')
    plt.gca().set_ylabel('position ($\mu m$)')

    # Move legend
    ax = plt.gca()
    ax.legend(bbox_to_anchor=(0., 1.02, 1., 0.5), loc='lower left',
              ncol=4, mode="expand", borderaxespad=0.)

    # plt.tight_layout()

    return fig


##################################################################################################

@numba.jit(nopython=True)
def count_kinds(data, Pop, i_min, i_max, np_types, n_time):

    for i in range(0, n_time):
        for j in np_types:
            mask = Pop[i_min[i]:i_max[i]] == j
            # return the number of cells of type j at "time" i
            number = np.count_nonzero(mask)
            data[i, j] = number

    return data


@treat_multiple_simulations
def f_kind_number(result):
    """
    Count the number of cells from each cell type for each time point.

    Parameters
    ----------
    result (Npz file or iterator of Npz file): simulation result (or iterator of simulation result).

    Returns
    -------
    data (Numpy array (n_time,n_cell_types + 1): the number of cells of each type. data[:,-1] is the total number of cells.
    """
    keys_pop = np.cumsum(result['sizepop'])

    pop_hd = param.pop_hd  # headers for result['pop']

    n_time = len(result['time'])
    n_cell = len(param.cell_types)
    data = np.zeros((n_time, n_cell+1))

    # Prepare variables for numba
    np_types = np.array([*param.cell_to_int.values()])
    np_Pop = result['pop'][:, pop_hd['type_c']]

    i_min = np.zeros(n_time, dtype=np.int32)
    i_max = np.zeros(n_time, dtype=np.int32)

    for i in range(n_time):
        if i == 0:
            i_min[0] = np.int32(0)
            i_max[0] = np.int32(keys_pop[0])
        else:
            i_min[i], i_max[i] = np.int32(
                keys_pop[i - 1]), np.int32(keys_pop[i])

    data = count_kinds(data, np_Pop, i_min, i_max, np_types, n_time)

    data[:, n_cell] = result['sizepop']

    return data


def plot_kind_number(result0):
    """
    Count and plot the number of cells from each cell type.

    Parameters
    ----------
    result : iterator.
        Result of a simulation saved from main.py. Or list of such results.

    Returns
    -------
    kinds_number : TYPE
        DESCRIPTION.
    fig : TYPE
        DESCRIPTION.

    """
    if hasattr(result0, '__next__'):
        multiple = True
        # copy original iterator for analysis + copy original iterator for plotting later
        result, result_2 = itertools.tee(result0)
        res = next(result_2)  # the result file used for plotting later.
    else:
        multiple = False
        res = result0
        result = result0

    kinds_number = f_kind_number(result)

    fig, ax = plt.subplots(1, 1)
    ax.grid(b=True, zorder=0)

    if multiple:
        tmp = np.asarray(kinds_number)
        ave = np.mean(tmp, axis=0)
        std = np.std(tmp, axis=0)
        ax.plot(res['time'], ave[:, -1],
                color='k', label='total', lw=1)
        ax.plot(res['time'], ave[:, -1] + std[:, -1],
                color='k', linestyle='dotted', lw=1)
        ax.plot(res['time'], ave[:, -1] - std[:, -1],
                color='k', linestyle='dotted', lw=1)

        for j, i in enumerate(param.cell_types):
            ax.plot(res['time'], ave[:, j],
                    color=color_code[i], label=i, lw=1.5)
            ax.plot(res['time'], ave[:, j] + std[:, j],
                    color=color_code[i], linestyle='dotted', lw=1)
            ax.plot(res['time'], ave[:, j] - std[:, j],
                    color=color_code[i], linestyle='dotted', lw=1)

    else:
        ax.plot(res['time'], kinds_number[:, -1],
                color='k', label='total', lw=1)

        for j, i in enumerate(param.cell_types):
            ax.plot(res['time'], kinds_number[:, j],
                    color=color_code[i], label=i, lw=1.5)

    ax.legend()

    ax.set_xlim(left=0)
    ax.set_ylim(bottom=0)
    ax.set_xlabel('time ($h$)')
    ax.set_ylabel('# of cells')

    plt.tight_layout()

    return kinds_number, fig


def stat_on_kind_number(data_kind):
    data_kind = np.asarray(data_kind)
    if len(data_kind.shape) == 2:
        print(
            f"Total number of cells at final time: {data_kind[-1][-1]}.")
        print(
            f"Total number of stem cells at final time: {data_kind[-1][param.cell_to_int['sc']]}.")
        print(
            f"Ratio of progenitors to total cells at final time: {data_kind[-1][param.cell_to_int['pc']]/data_kind[-1][-1]}.")
        print(
            f"Ratio of goblet cells to enterocytes at final time: {data_kind[-1][param.cell_to_int['gc']]/data_kind[-1][param.cell_to_int['ent']]}.")

    # several simulations: we do average and standard deviation.
    elif len(data_kind.shape) == 3:
        # Total number of cells
        N_total = []

        # Stem cells
        N_sc = []

        # Ratio of progenitors to total cells
        R_prog = []

        # Ratio of goblets to enterocytes
        R_gob = []

        for sim in data_kind:
            N_total.append(sim[-1][-1])
            N_sc.append(sim[-1][param.cell_to_int['sc']])
            R_prog.append(sim[-1][param.cell_to_int['pc']]/sim[-1][-1])
            R_gob.append(sim[-1][param.cell_to_int['gc']] /
                         sim[-1][param.cell_to_int['ent']])
        N_total = np.asarray(N_total)
        N_sc = np.asarray(N_sc)
        R_prog = np.asarray(R_prog)
        R_gob = np.asarray(R_gob)

        print(
            f"Total number of cells at final time: {np.mean(N_total)} +- {np.std(N_total)}.")
        print(
            f"Total number of stem cells at final time: {np.mean(N_sc)} +- {np.std(N_sc)}.")
        print(
            f"Ratio of progenitors to total cells at final time: {np.mean(R_prog)} +- {np.std(R_prog)}.")
        print(
            f"Ratio of goblet cells to enterocytes at final time: {np.mean(R_gob)} +- {np.std(R_gob)}.")
    else:
        raise IndexError(
            "Shape of data_kind does not correspond to a known case.")


#####################################################################################

def f_cell_trajectory(result, cells_id, Tstart=None, Tend=None):
    """Return the successive spatial position (and corresponding times) of each cell in cells_id between Tstart and Tend.

    Args:
        result (Npz object): simulation result.
        cells_id (iterable of int or float): cells_id for which to plot trajectory.
        Tstart (float, optional): Starting time. If None, Tstart is the starting time of result. Defaults to None.
        Tend (float, optional): End time. If None, Tend is the end time of result. Defaults to None.

    Returns:
        l_positions (list (len(cells_id)) of list of floats): The successive positions of each cell in cells_id.
        l_times (list (len(cells_id)) of list of floats): The time correspond to each position of l_positions. Same shape as l_positions.
    """

    result = slice_res(result, Tstart, Tend)

    keys_pop = np.cumsum(result['sizepop'])

    id_last_cell = int(np.amax(result['pop'][:, pop_hd['id']]))

    if max(cells_id) > id_last_cell:
        warnings.warn(
            f'cells_id values are greater than last cell {id_last_cell}')

    cells_id = cells_id[cells_id <= id_last_cell]

    # Retrieve data
    dic_celllife = f_celllife(cells_id, result)
    l_positions = dic_celllife['pos']
    l_times = dic_celllife['times']

    return l_positions, l_times


def plot_cell_trajectory(result, cells_id, Tstart=None, Tend=None):
    """Return the successive spatial position (and corresponding times) of each cell in cells_id between Tstart and Tend, and the associated plot.

    Args:
        result (Npz object): simulation result.
        cells_id (iterable of int or float): cells_id for which to plot trajectory.
        Tstart (float, optional): Starting time. If None, Tstart is the starting time of result. Defaults to None.
        Tend (float, optional): End time. If None, Tend is the end time of result. Defaults to None.

    Returns:
        l_positions (list (len(cells_id)) of list of floats): First returned value of f_cells_trajectory(result, cells_id, Tstart, Tend).
        l_times (list (len(cells_id)) of list of floats): Second returned value of f_cells_trajectory(result, cells_id, Tstart, Tend).
        fig (matplotlib figure): Plot of the spatial trajectories of cells.
    """

    if Tstart is None:
        idx_s = 0
    else:
        idx_s = np.where(result['time'] >= Tstart)[0][0]
    if Tend is None:
        idx_e = -1
    else:
        idx_e = np.where(result['time'] >= Tend)[0][0]

    l_positions, l_times = f_cell_trajectory(result, cells_id, Tstart, Tend)

    # Plot
    # Trajectory of cells
    fig, ax = plt.subplots()
    ax.grid(b=True, zorder=0)
    for i, j in enumerate(cells_id):
        plt.plot(l_times[i], l_positions[i], label='cell '+str(j), lw=1)

    # Set up the rest of the figure
    # Axis marking top and bottom of the crypt
    plt.ylim(0-4, param.z_max+4)
    xmin, xmax, *foo = plt.axis()
    plt.plot([xmin, xmax], 0*np.ones(2), '--', c='grey', lw=1.5)
    plt.plot([xmin, xmax], param.z_max*np.ones(2), '--', c='grey', lw=1.5)
    plt.margins(x=0)
    # Axis title
    plt.gca().set_xlabel(r'time ($h$)')
    plt.gca().set_ylabel(r'position ($\mu m$)')

    plt.tight_layout()

    return l_positions, l_times, fig


###############################################################################

@treat_multiple_simulations
def f_jump_spatial_repartition(result, n_bins=10, dens=True):
    """Compute the spatial repartition of jumps.

    Args:
        result (Npz object or iterator of Npz object): simulation result (or iterator of simulation result).
        n_bins (int, optional): number of spatial bins. Defaults to 10.
        dens (bool, optional): For each type of jump, plot either the total number of jump per bin (false) or the
        values normalized acroos all bins to 1 (true). Defaults to True.

    Returns:
        Y (Numpy array of size (n_cell_types,n_jump_types,n_bins) if result is not an iterator, of size (n_iterator,n_cell_types,n_jump_types,n_bins)
        if result is an iterator): histogram values.
    """

    jmp_hd = param.jmp_hd  # headers for result['jump']
    n_cell_types = len(param.cell_types)
    n_jump_types = len(param.jump_types)

    bin_width = param.z_max / n_bins

    # Construct data
    Y = np.empty((n_cell_types, n_jump_types, n_bins))  # Histo

    for i_num, i in enumerate(param.cell_types):
        for j_num, j in enumerate(param.jump_types):
            mask = (result['jump'][:, jmp_hd['type_mom']] == i_num) \
                & (result['jump'][:, jmp_hd['type_jump']] == j_num)
            X = result['jump'][mask, jmp_hd['pos_mom']]
            if len(X) == 0:
                Y[i_num, j_num, :] = 0.
            else:
                Y[i_num, j_num, :] = bin_width * np.histogram(
                    X, bins=n_bins, range=(0., param.z_max), density=dens)[0]
    return Y


def plot_jump_spatial_repartition(result0, n_bins=10, dens=True):
    """Plot the spatial repartition of jumps.

    Args:
        result (Npz object or iterator of Npz object): simulation result (or iterator of simulation result).
        n_bins (int, optional): number of spatial bins. Defaults to 10.
        dens (bool, optional): For each type of jump, plot either the total number of jump per bin (false) or the
        values normalized acroos all bins to 1 (true). Defaults to True.

    Returns:
        X (Numpy array of size (n_cell_types,n_jump_types,n_bins) if result is not an iterator, of size (n_iterator,n_cell_types,n_jump_types,n_bins)
        if result is an iterator): histogram values.
        fig (Matplotlib figure)
    """

    # Test if result is an iterator (multiple simulations)
    if hasattr(result0, '__next__'):
        multiple = True
        # copy original iterator
        result, _ = itertools.tee(result0)
    else:
        result = result0
        multiple = False

    n_cell_types = len(param.cell_types)
    n_jump_types = len(param.jump_types)

    X = f_jump_spatial_repartition(result, n_bins, dens)

    # Get average and standard deviation if multiple simulations
    if multiple:
        X_mean = np.mean(X, axis=0)
        X_std = np.std(X, axis=0)

    if n_cell_types > 1:
        n_col = 2
    else:
        n_col = 1

    n_row = int(np.ceil(n_cell_types/n_col))
    fig, axes = plt.subplots(n_row, n_col, sharex='all', figsize=(14.0, 10.))

    center_bins = (np.linspace(0., param.z_max, n_bins + 1)[
                   :-1] + np.linspace(0., param.z_max, n_bins + 1)[1:])/2
    bin_width = param.z_max/n_bins

    for i_num, i in enumerate(param.cell_types):
        # Treat case with only 1 type of cell
        if n_cell_types > 1:
            ax = axes.flatten()[i_num]
        else:
            ax = axes

        ax.grid(b=True, zorder=0)
        ax.set_title(cells_name[i])

        for j, type_j in enumerate(param.jump_types):
            color = color_code[type_j]
            label = type_j

            if multiple:
                ax.bar(center_bins, X_mean[i_num, j, :], width=bin_width, label=label,
                       color=color, edgecolor='k', alpha=0.5, yerr=X_std[i_num, j, :], zorder=10)

            else:
                ax.bar(center_bins, X[i_num, j, :], label=label,
                       color=color, edgecolor='k', alpha=0.5, width=bin_width, zorder=10)

        ax.set_ylim(0.)

        # Set legend
        if i_num == 0:
            ax.legend()
            ax.set_xticks(np.linspace(0, param.z_max,
                          n_bins + 1))  # set x ticks
            ax.set_ylabel('# of jumps')

    total_number_ax = n_row*n_col
    for j_num in range(total_number_ax - n_col, total_number_ax):  # x_axis label on bottom row

        # Treat case with only 1 type of cell
        if n_cell_types > 1:
            ax = axes.flatten()[j_num]
        else:
            ax = axes

        ax.set_xlabel('position ($\mu m$)')

    if (n_cell_types % 2 == 1) & (n_cell_types != 1):
        axes.flatten()[-1].axis('off')

    return X, fig


###############################################################################
@treat_multiple_simulations
def f_bin_position(result, T=None, n_bins=20):
    """Return spatial histogram at time T of the cell population according to cell type.

    Args:
        result(Npz object or iterator of Npz object): simulation result or iterator of simulation results.
        T(float, optional): Time to plot. If None, the last time of result is used. Defaults to None.
        n_bins(int, optional): [description]. Defaults to 20.

    Returns:
        If result is an iterator:
            Data(list(n_simulations) of numpy array(n_cell_types, n_bins)): Histogram values.

        If result is not an iterator:
            X(numpy array(n_cell_types, n_bins)): Histogram values.
    """

    n_cell = len(param.cell_types)
    pop_hd = param.pop_hd

    if T is None:
        idx_t = result['time'].shape[0] - 1
        t = result['time'][idx_t]
    else:
        idx_t = np.argmin(result['time'] - T)
        t = result['time'][idx_t]

    keys_pop = np.cumsum(result['sizepop'])

    X = np.zeros((n_cell, n_bins))

    i_min, i_max = int(keys_pop[idx_t - 1]), int(keys_pop[idx_t])

    for j, type_c in enumerate(param.cell_types):

        mask = result['pop'][i_min:i_max, pop_hd['type_c']
                             ] == param.cell_to_int[type_c]
        position = result['pop'][i_min:i_max, pop_hd['pos']][mask]

        X[j, :] = np.histogram(
            position, bins=n_bins, range=(0., param.z_max))[0]

    return X


def plot_position(result0, T=None, n_bins=20):
    """Return spatial histogram at time T of the cell population according to cell type, and the associated plot.

    Args:
        result(Npz object or iterator of Npz object): simulation result or iterator of simulation results.
        T(float, optional): Time to plot. If None, the last time of result is used. Defaults to None.
        n_bins(int, optional): [description]. Defaults to 20.

    Returns:
        positions(numpy array(n_simulations, n_cell_types, n_bins)): return value of call to fbins_position(result, T, n_bins), converted to a numpy array.
        fig(Matplotlib figure): associated figure.
    """
    if hasattr(result0, '__next__'):
        multiple = True
        # copy original iterator + a result file to get times for plotting later.
        result, result_cp = itertools.tee(result0)

        # Display T in figure
        if T is None:
            res = next(result_cp)  # used for displaying T in plot
            idx_t = res['time'].shape[0] - 1
            T_legend = res['time'][idx_t]
        else:
            T_legend = T

    else:
        result = result0
        multiple = False
        # Display T in figure
        if T is None:
            idx_t = result0['time'].shape[0] - 1
            T_legend = result0['time'][idx_t]
        else:
            T_legend = T

    # Produce data
    if multiple:
        positions = np.array(f_bin_position(result, T, n_bins))
        Average = np.mean(positions, axis=0)
        Std = np.std(positions, axis=0)
    else:
        positions = f_bin_position(result, T, n_bins)
        Average = positions
        Std = np.zeros(Average.shape)

    # Plotting
    bins = np.linspace(0, param.z_max, n_bins + 1)
    center = (bins[1:] + bins[:-1])/2
    width = center[1] - center[0]
    pad = 5  # in points
    n_row = len(param.cell_types)
    n_col = 1
    fig, axes = plt.subplots(n_row, n_col, sharex='all',
                             sharey='row', figsize=(6.2, 7.))

    for i_cell, cell in enumerate(param.cell_types):
        axe = axes.flatten()[i_cell]
        axe.grid(b=True, axis='y', zorder=0)
        axe.bar(center, Average[i_cell, :], yerr=Std[i_cell, :], width=width, ecolor='k',
                color=color_code[cell], edgecolor='k', zorder=3)

        if i_cell == 0:
            axe.set_ylabel(r'# ' + cells_name[cell])
        else:
            axe.set_ylabel(cells_name[cell])

        if i_cell == len(param.cell_types)-1:
            axe.set_xlabel(r'position ($\mu m$)')

        if i_cell == 0:
            axe.set_xticks(bins)
            labels = [round(x) for x in bins]
            for i, l in enumerate(labels):
                if i % 5 != 0:
                    labels[i] = ''
            axe.set_xticklabels(labels)

            axe.annotate(r'$t = {}~h$'.format(int(T_legend)), xy=(0.5, 1), xytext=(0, pad),
                         xycoords='axes fraction', textcoords='offset points',
                         size=16, ha='center', va='baseline')
    plt.tight_layout()

    return positions, fig


###############################################################################

@treat_multiple_simulations
def f_turnover(result, T, cell_type=(("all",),)):
    """Compute the renewal rate of the epithelium over time. Compute the fraction of the number of marked cells remaining
        over the initial number of marked cells. . Upon division of
    a marked cells, the label has 0.5 probability to pass from the mother cell to the daughter cell.

    Parameters
    ----------
    result: Npz object or iterator of Npz object.
        Simulation result or iterator of simulation results.
    T: float.
        Starting time from which to compute renewal dynamic. Time when cells are marked.
    cell_type: iterable of iterable of stringsz, optional. The default is cell_type=("all",).
        Each element is a tuple of cell types. Specify
        which cells are marked initially. For example, if one wants to mark stem cells and progenitor cells, and also only
        stem cells, we do: cell_types=(('sc', 'pc'),('sc',)). If default, all cells are marked.

    Returns
    -------
    out: dictionnary.
    Each key is one of element of cell_types. For each attribute, first element is an numpy array of time points, second element is a numpy array
    of turnover values for said time points.
    """

    pop_hd = param.pop_hd
    jmp_hd = param.jmp_hd

    keys_pop = np.cumsum(result['sizepop'])

    turnover_val = []
    tim = []  # time

    t_final = result['time'][-1]

    #######################################################################
    # Compute the fraction of marked cells left from the initial pool of marked cells
    out = {}
    for types in cell_type:

        # Find closest time
        index = np.argmin(np.abs(result['time'] - T))
        t_init = result['time'][index]

        # Save state of the population at closest time
        if index == 0:
            i_min, i_max = 0, int(keys_pop[index])
        else:
            i_min, i_max = int(
                keys_pop[index - 1]), int(keys_pop[index])

        tmp = result['pop'][i_min:i_max, pop_hd['id']]

        if types != "all":
            c_types = [param.cell_to_int[x] for x in types]
            c_types = np.asarray(c_types)
            mask = np.isin(
                result['pop'][i_min:i_max, pop_hd['type_c']], c_types)
            pop_0 = tmp[mask]
        elif types == "all":
            pop_0 = tmp

        else:
            raise ValueError("Unknown cell_types.")

        size_0 = pop_0.shape[0]
        size = pop_0.shape[0]

        # Find first jump time after
        indexes = np.where(result['jump'][:, jmp_hd['t']] >= t_init)

        if len(indexes[0]) != 0:
            idx = indexes[0][0]

        else:  # there are no jumps after this time
            # We add a point at the end, with value = 1 (no turn-over)
            t_final = result['time'][-1]
            tim.append([T, t_final])
            turnover_val.append([1., 1.])

            return tim, turnover_val

        # Compute turnover
        times = result['jump'][idx:, jmp_hd['t']]
        times = np.concatenate((np.array([t_init]), times))
        turn = np.zeros(times.shape[0])
        turn[0] = 1.

        x = 1.
        k = idx
        k_max = result['jump'].shape[0]

        while (k < k_max) & (x != 0):

            # Check for division
            # True if a division
            if not np.isnan(result['jump'][k, jmp_hd['id_daug']]):

                # If the cell is marked
                if result['jump'][k, jmp_hd['id_mom']] in pop_0:
                    tmp = np.random.binomial(1, 0.5)
                    # if division of a pop_0 cell we randomly choose to switch the marking to daughter cell or not
                    if bool(tmp):
                        idx_cell = np.where(
                            pop_0 == result['jump'][k, jmp_hd['id_mom']])[0]
                        pop_0[idx_cell[0]] = result['jump'][k,
                                                            jmp_hd['id_daug']]

            # Check for extrusion
            elif result['jump'][k, jmp_hd['type_jump']] == param.jump_to_int['ex']:

                # If the cell is marked
                if result['jump'][k, jmp_hd['id_mom']] in pop_0:
                    size += -1

            x = size / size_0
            turn[k-idx + 1] = x
            k += 1

        turnover_val += list(turn)
        tim += list(times)

        # Last data point for final time
        tim.append(t_final)
        turnover_val.append(turnover_val[-1])

        # Length of turnover_val is possibly big, so we select only 250 (max) data points
        filtr = np.linspace(0, len(tim)-1, 250, dtype='int')
        filtr = np.unique(filtr)
        filtr = list(filtr)

        tim = [tim[i] for i in filtr]
        turnover_val = [turnover_val[i] for i in filtr]

        out[types] = [tim, turnover_val]

    return out


def plot_turnover(result0, T, cell_type=("all",)):
    """Plot renewal rate of the epithelium using f_turnover(result, T, cell_type).

    Args:
        result (Npz object or iterator of Npz object): simulation result or iterator of simulation results.
        T (float): starting time from which to compute renewal dynamic. Time when cells are marked.
        cell_type (iterable of iterable of strings, optional): Defaults to ("all",). Each element is a tuple of cell types. Specify
        which cells are marked initially. For example, if one wants to mark stem cells and progenitor cells, and also only
        stem cells, we do: cell_types=(('sc', 'pc'),('sc',)). If default, all cells are marked.

    Returns:
        Y (dictionnary): output of f_turnover().
        fig (Matlpotlib figure): corresponding figure.
    """
    # Test if result is an iterator (multiple simulations)
    if hasattr(result0, '__next__'):
        multiple = True
        # copy original iterator
        _, result = itertools.tee(result0)
    else:
        multiple = False
        result = result0

    # Construct data
    X = f_turnover(result, T, cell_type)

    if multiple:
        Y = {}
        Y_std = {}
        for i_c, c_type in enumerate(cell_type):
            global t_snap
            t_snap = np.linspace(T, X[0][c_type][0][-1], 250)
            renew_t_snap = np.zeros((len(X), *t_snap.shape))
            for i_x, x in enumerate(X):
                id_t_snap = np.searchsorted(
                    x[c_type][0], t_snap, side="left")
                id_t_snap = [min(int(i), len(x[c_type][1])-1)
                             for i in id_t_snap]
                renew_t_snap[i_x, :] = np.asarray(
                    [x[c_type][1][i] for i in id_t_snap])

            Y[c_type] = np.mean(renew_t_snap, axis=0)
            Y_std[c_type] = np.std(renew_t_snap, axis=0)

        # Plotting
        fig, ax = plt.subplots()
        fig.suptitle('Renewal rate of the crypt.')
        ax.set_xlabel('time ($h$)')
        ax.set_ylabel('fraction')
        ax.set_ylim(ymin=0.)
        ax.grid(b=True, zorder=0)

        for c_type in Y.keys():

            if c_type == ("all",):
                ax.plot(t_snap, Y[c_type],
                        color='k', label=c_type)
                ax.plot(t_snap, Y[c_type] + Y_std[c_type],
                        color='k', linestyle='dotted')
                ax.plot(t_snap, Y[c_type] - Y_std[c_type],
                        color='k', linestyle='dotted')
            else:
                label = "+".join(c_type)
                line, = ax.plot(t_snap, Y[c_type], label=label)
                ax.plot(t_snap, Y[c_type] + Y_std[c_type],
                        color=line.get_color(), linestyle='dotted')
                ax.plot(t_snap, Y[c_type] - Y_std[c_type],
                        color=line.get_color(), linestyle='dotted')

        ax.legend()

    else:
        Y = X
        # Plotting
        fig, ax = plt.subplots()
        fig.suptitle('Renewal rate of the crypt.')
        ax.set_xlabel('time ($h$)')
        ax.set_ylabel('fraction')
        ax.set_ylim(ymin=0.)
        ax.grid(b=True, zorder=0)

        for c_type in X.keys():
            if c_type == ("all",):
                ax.plot(Y[c_type][0], Y[c_type][1],
                        color='k', label=c_type)
            else:
                label = "+".join(c_type)
                ax.plot(Y[c_type][0], Y[c_type]
                        [1], label=label)

        ax.legend()

    return X, fig


######################################################################################

def test_jumps(res_jmp):
    """ Check if the array contains jumps or is just None (=empty). """
    if res_jmp[0, param.jmp_hd['t']] is None:

        return False

    else:

        return True


@treat_multiple_simulations
def count_events(result, t_0=None, t_1=None):
    """
    Count the number of events of each type between t_0 and t_1.

    Parameters
    ----------
    result: Npz object or iterator of Npz object.
        Simulation result or iterator of simulation results.
    t_0: float, optional
        Starting time. If None, t_0 is the first time. The default is None.
    t_1: float, optional
        End time. If None, t_1 is the final time. The default is None.

    Returns
    -------
    X_dct: nested dict
        First key is cell type and second key is the type of jump. For each type of event, count the number of event between t_0 and t_1.
    """
    if t_0 is None:
        t_0 = result['time'][0]
    if t_1 is None:
        t_1 = result['time'][-1]  # final time

    n_types = len(param.cell_types)
    n_jumps = len(param.jump_types)

    X = np.zeros((n_types, n_jumps))

    if test_jumps(result['jump']):  # Test if jumps are present
        # first jump after t_0
        idx_s = np.searchsorted(result['jump'][:, param.jmp_hd['t']], t_0)
        # first jump after t_1
        idx_e = np.searchsorted(result['jump'][:, param.jmp_hd['t']], t_1)

        col_mom = param.jmp_hd['type_mom']
        col_j = param.jmp_hd['type_jump']

        for i in range(n_types):
            for j in range(n_jumps):
                mask = (result['jump'][idx_s:idx_e, col_mom] == i) & \
                    (result['jump'][idx_s:idx_e, col_j] == j)
                n = np.sum(mask)
                X[i, j] = n

    X_dct = {}
    for i, cell in enumerate(param.cell_types):
        Y = {}
        for j, jump in enumerate(param.jump_types):
            Y[jump] = X[i, j]

        X_dct[cell] = Y.copy()

    return X_dct


@treat_multiple_simulations
def cell_type_duration(result, cell_type, t_0=None, t_1=None):
    """Returns the total time spend in a given cell_type during a simulation. That is to say, the addition of the time each cell spend in the 
    state cell_type between t_0 and t_1.

    Args:
        result (Npz object or iterator of Npz object): simulation result or iterator of simulation results.
        cell_type (str): cell type.
        t_0 (float, optional): Starting time. If None, it is the starting time. Defaults to None.
        t_1 (float, optional): Ending time. If None, it is the final time. Defaults to None.

    Returns:
        out (float): the total time spend in cell_type.
    """
    keys_pop = np.cumsum(result['sizepop'])
    id_type = param.cell_to_int[cell_type]
    id_jump_birth = param.jump_to_int[cell_type]

    if t_0 is None:
        t_0 = 0
    if t_1 is None:
        t_1 = result['time'][-1]  # final time

    # Find the closest time t_0 where the state of the total population was saved
    idx_0 = np.argmin(np.abs(result['time'][:] - t_0))
    t_0 = result['time'][idx_0]

    # Find the closest time t_1 where the state of the total population was saved
    idx_1 = np.argmin(np.abs(result['time'][:] - t_1))
    t_1 = result['time'][idx_1]

    # Store state of the population at t_0 and t_1
    if idx_0 == 0:
        idx_0_0 = 0
        idx_0_1 = keys_pop[idx_0]

    else:
        idx_0_0 = keys_pop[idx_0 - 1]
        idx_0_1 = keys_pop[idx_0]

    pop_0 = result['pop'][idx_0_0:idx_0_1, :]

    idx_1_0 = keys_pop[idx_1 - 1]
    idx_1_1 = keys_pop[idx_1]
    pop_1 = result['pop'][idx_1_0:idx_1_1, :]

    T_b = 0.
    T_d = 0.

    # Add cells that were already born at t_0
    N = np.sum(pop_0[:, param.pop_hd['type_c']] == id_type)
    tmp = t_0 * N
    T_b += tmp

    if result['jump'][0][0] is not None:  # there was a jump

        # first jump after t_0
        idx_s = np.searchsorted(result['jump'][:, param.jmp_hd['t']], t_0)
        # first jump after t_1
        idx_e = np.searchsorted(result['jump'][:, param.jmp_hd['t']], t_1)
        Jumps = result['jump'][idx_s:idx_e, :]

        # find birth event
        mask = Jumps[:, param.jmp_hd['type_jump']] == id_jump_birth
        T_b += np.sum(Jumps[mask, param.jmp_hd['t']])

        # find differenciation/death events
        warnings.warn("This function works only with symetric division event.")
        mask = (Jumps[:, param.jmp_hd['type_mom']] == id_type) \
            & (Jumps[:, param.jmp_hd['type_jump']] != id_jump_birth)  # this part of the code works ONLY for particular type of jumps (symetric division)
        T_d += np.sum(Jumps[mask, param.jmp_hd['t']])

    # Add cells that are still alive at t_1
    N = np.sum(pop_1[:, param.pop_hd['type_c']] == id_type)
    tmp = t_1 * N
    T_d += tmp

    out = T_d - T_b

    return out


def rate_sc_division(result0, t_0=None, t_1=None):
    """Compute symetric division rate of progenitor cells.

    Args:
        result (Npz object or iterator of Npz object): simulation result or iterator of simulation results.
        t_0 (float, optional): Ending time. If None, first time. Defaults to None.
        t_1 (float, optional): Starting time. If None, final time. Defaults to None.

    Returns:
        r_div_sc (float): division rate of stem cells.
    """
    if hasattr(result0, '__next__'):
        multiple = True
        # copy original iterator + a result file to get times for plotting later.
        result, result_2 = itertools.tee(result0)
    else:
        multiple = False
        result = result0

    if not multiple:
        # count the number of events of each type between t_0 and t_1
        n_events = count_events(result, t_0=None, t_1=None)
        time_as_sc = cell_type_duration(result, 'sc', t_0=None, t_1=None)
        r_div_sc = n_events['sc']['sc']/time_as_sc
        print(f"Rate of stem cell division: {r_div_sc:.3f} .")

    else:
        # count the number of events of each type between t_0 and t_1
        n_events = count_events(result, t_0=None, t_1=None)
        time_as_sc = cell_type_duration(result_2, 'sc', t_0=None, t_1=None)

        r_div_sc = np.asarray([n_events[i]['sc']['sc']/time_as_sc[i]
                              for i in range(len(n_events))])
        print(
            f"Rate of stem cell division: {np.mean(r_div_sc):.3f} +- {np.std(r_div_sc):.3f}.")

    return r_div_sc


def rate_pc_division(result0, t_0=None, t_1=None):
    """Coùpute symetric division rate of progenitor cells.

    Args:
        result (Npz object or iterator of Npz object): simulation result or iterator of simulation results.
        t_0 (float, optional): Ending time. If None, first time. Defaults to None.
        t_1 (float, optional): Starting time. If None, final time. Defaults to None.

    Returns:
        r_div_pc (float): division rate of progenitor cells.
    """
    if hasattr(result0, '__next__'):
        multiple = True
        # copy original iterator + a result file to get times for plotting later.
        result, result_2 = itertools.tee(result0)
    else:
        multiple = False
        result = result0

    if not multiple:
        # count the number of events of each type between t_0 and t_1
        n_events = count_events(result, t_0=None, t_1=None)
        time_as_pc = cell_type_duration(result, 'pc', t_0=None, t_1=None)
        r_div_pc = n_events['pc']['pc']/time_as_pc
        print(f"Rate of progenitor cell division: {r_div_pc:.3f} .")

    else:
        # count the number of events of each type between t_0 and t_1
        n_events = count_events(result, t_0=None, t_1=None)
        time_as_pc = cell_type_duration(result_2, 'pc', t_0=None, t_1=None)

        r_div_pc = np.asarray([n_events[i]['pc']['pc']/time_as_pc[i]
                              for i in range(len(n_events))])
        print(
            f"Rate of progenitor cell division: {np.mean(r_div_pc):.3f} +- {np.std(r_div_pc):.3f}.")

    return r_div_pc

######################################################################################


def fmigr_speed(result, keys_pop, zmin=50, zmax=175):
    """ Return a dictionnary with average speed and weighted average speed, of migration
    between zmin and zmax.
    First coordinate is average of every speed. The second coordinate average is done by averaging all speed of cells,
    each speed having a weight proportionnal to the time the cell spend between zmin and zmax.

    For returning speed, we compute the distance the cell travelled and the time it
    took...
    """

    # Construct data: retrieve list of positions for all cells.
    cell_number = np.unique(result['pop'][:, pop_hd['id']])
    dic = f_celllife(cell_number, result, keys_pop)
    pos = dic['pos']  # list where element i is an 1D array of cell i positions
    times = dic['times']  # list where element i is an 1D array of cell i times

    Data = []
    ######################
    # Compute average speed
    tmp = 0
    for i_c, idx_c in enumerate(cell_number):
        try:
            i0 = np.where((pos[i_c] >= zmin) & (pos[i_c] <= zmax))[0][0]
        except IndexError:  # no match where found
            continue  # move to the next iteration of for loop

        try:
            i1 = np.where(pos[i_c] >= zmax)[0][0]
        except IndexError:  # take last position of cell i
            i1 = len(times[i_c]) - 1

        if i0 == i1:
            continue

        t = times[i_c][i1] - times[i_c][i0]
        distance = pos[i_c][i1] - pos[i_c][i0]
        if t > 1e-11:
            speed = distance/t
        else:
            continue

        tmp += 1
        Data.append([t, distance, speed])

    Data = np.asarray(Data)
    speed = np.sum(Data[:, 2], axis=0)/Data.shape[0]

    smart_speed = np.sum(Data[:, 1], axis=0)/np.sum(Data[:, 0], axis=0)
#    print('Vitesses: ', speed,smart_speed)
    dic = {}
    dic['ave'] = speed
    dic['w'] = smart_speed  # weighted

    return dic


#####################################################################################################

def plot_concentration_at_t(result0, T=None):
    """Plot concentration of each chemical at time T.

    Args:
        result (Npz object or iterator of Npz object): simulation result or iterator of simulation results.
        T (float, optional): Time at which concentrations are plotted. If None, it is the final time. Defaults to None.

    Returns:
        fig (Matplotib figure).
    """

    if hasattr(result0, '__next__'):
        multiple = True
        # copy original iterator
        result, _ = itertools.tee(result0)
    else:
        multiple = False
        result = result0

    N_conc = []
    global legend_time
    if multiple:
        Data = []
        for i_res, res in enumerate(result):
            if T is None:
                idx_T = -1
                legend_time = res['time'][idx_T]
            else:
                idx_T = np.argmin(np.abs(res['time'] - T))
                legend_time = T

            Conc = res['conc'][idx_T]
            Data.append(Conc)

        Data = np.asarray(Data)
        Average_conc = np.mean(Data, axis=0)
        Dev_conc = np.std(Data, axis=0)

    else:
        if T is None:
            idx_T = -1
            legend_time = result['time'][idx_T]
        else:
            idx_T = np.argmin(np.abs(result['time'] - T))
            legend_time = T

    # Plot
    n_chem = len(param_c.chem)
    n_col = 2
    n_row = int(np.floor((n_chem + 1) / 2))
    fig, axes = plt.subplots(nrows=n_row, ncols=n_col, sharex='all')
    fig.set_figwidth(7.5)
    points = param_c.coord_mesh_x

    if multiple:
        for i_c, chem in enumerate(param_c.chem):
            ax = axes.flatten()[i_c]
            ax.grid(b=True, zorder=0)
            ax.plot(points, Average_conc[param_c.chem_to_int[chem]], linestyle='solid',
                    label=param_c.chem_full[chem], color=color_code[chem])
            ax.plot(points, Average_conc[param_c.chem_to_int[chem]] + Dev_conc[param_c.chem_to_int[chem]],
                    linestyle='dashed', linewidth=0.7, color=color_code[chem])
            ax.plot(points, Average_conc[param_c.chem_to_int[chem]] - Dev_conc[param_c.chem_to_int[chem]],
                    linestyle='dashed', linewidth=0.7, color=color_code[chem])

            ax.legend()
            if i_c % 2 == 0:
                ax.set_ylabel(r"concentration")

            if i_c // 2 == n_row - 1:
                ax.set_xlabel(r'position ($\mu m$)')

    else:
        for i_c, chem in enumerate(param_c.chem):
            ax = axes.flatten()[i_c]
            ax.grid(b=True, zorder=0)
            ax.plot(points, result['conc'][idx_T][param_c.chem_to_int[chem]], linestyle='solid',
                    label=param_c.chem_full[chem], color=color_code[chem])

            ax.legend()
            if i_c % 2 == 0:
                ax.set_ylabel(r"concentration")

            if i_c // 2 == n_row - 1:
                ax.set_xlabel(r'position ($\mu m$)')

    fig.suptitle(f'$t ={legend_time:.1f}~h$')

    return fig
