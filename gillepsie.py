#!/usr/bin/env python3
# -*- coding: utf-8 -*-


####################################
######     GILLEPSIE     ###########
####################################

import numpy as np

import parameters as param
import jump_rates as rates


def Gillepsie_jump_time(population, arg_K, b_CI):
    """
    This function returns a proposition for the time of next jump, using a harsh majoration of the jump rate
    each cell.
    """
    u0 = np.random.random_sample()
    rate_max_per_cell = rates.f_rates_max(population)

    rate_max = np.sum(rate_max_per_cell)
    time_interval = -np.log(u0)/rate_max  # time interval before next jump

    return time_interval  # Q_max is then used to check if the jump is accepted or not


def Gillepsie_jump_type(population, concentration, arg_K, b_CI):
    """
    This function accepts or rejects the proposed jump. If the jump is accepted,
    it returns the cell that jumps and the type of jump it undergoes. If the jump is rejected,
    the function returns np.nan.
    """

    # Need to compute again rates.f_rates_max(population). One cannot use the value returned
    # computed before by Gillepsie_jump_time() as ordre of cells has been changed during evolution
    # of cell position
    r_max_per_cell = rates.f_rates_max(population)
    r_max = np.sum(r_max_per_cell)

    # Draw a cell
    proba = r_max_per_cell/r_max
    n_pop = population.shape[0]
    line_cell = np.random.choice(np.arange(0, n_pop), 1, p=proba)[0]
    cell = population[line_cell]

    # Draw a jump
    type_c = param.int_to_cell[cell[param.pop_hd['type_c']]]
    jumps, proba_j = list(zip(*rates.rates_max[type_c].items()))
    proba_j = np.asarray(proba_j)/sum(rates.rates_max[type_c].values())
    jump = np.random.choice(jumps, 1, p=proba_j)[0]

    # Can still be improved by evaluating just one jump
    r_cell = rates.f_rates(cell, population, concentration, arg_K, b_CI=b_CI)

    u1 = np.random.random_sample()

    # Accept or reject jump
    if r_cell[param.jump_to_int[jump]]/rates.rates_max[type_c][jump] >= u1:
        cell_line = line_cell
        jump_type = jump

    else:  # the jump proposition is rejected
        cell_line = np.nan
        jump_type = ""

    return cell_line, jump_type
