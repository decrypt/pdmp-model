#!/usr/bin/env python3
# -*- coding: utf-8 -*-


# ******************************************************************************

# We use an semi-implicit scheme to ensure positivity of the solutions.

# Call init_reaction() and return all the object that will be used to simulate
# reaction.

# init_reaction() calls construct_reac_speed() for each chemical and each reaction.

# ******************************************************************************


import numpy as np
import numba

import copy

import parameters as param
import parameters_chem as param_c


chem_to_int = param_c.chem_to_int
shape_conc = param_c.shape_conc  # shape of the concentration array


###############################################################################
# Main function

def init_reaction(Stoechio=param_c.Stoechio, chem=param_c.chem, reac=param_c.reac,
                  chem_to_int=param_c.chem_to_int, reac_to_int=param_c.reac_to_int):
    """
    From a list of chemicals (chem), reactions (reac) and stoechiometric coefficients (Stoechio),
    generates two nested list, func_reac_speed_impli and func_reac_speed_expli.
    Each element a_{i,j} of those nested list is itself a function: the reaction term
    of a given reaction for a given chemical. For a given reaction, we have to construct different reaction
    functions for each chemicals due to differences in the stoechiometry and discretization in time
    (implicit or explicit term). Implicit functions are for reaction term
    where the chemical is consumed, explicit functions are for reaction terms where the chemical is produced.

    Each of these functions is constructed automatically by calling the function
    construct_reac_speed_f() .
    """

    func_reac_speed_impli = [[] for i in range(len(chem))]
    func_reac_speed_expli = [[] for i in range(len(chem))]

    for i_chm in chem_to_int.values():
        for i_rc in reac_to_int.values():

            if Stoechio[i_rc, i_chm] < 0:  # the chemical is consumed
                impli = True
#                print(imp_or_exp)
                func_reac_speed_impli[i_chm].append(
                    [construct_reac_speed(i_chm, i_rc, impli), i_rc])

            elif Stoechio[i_rc, i_chm] > 0:  # the chemical is produced
                impli = False
#                print(imp_or_exp)
                func_reac_speed_expli[i_chm].append(
                    [construct_reac_speed(i_chm, i_rc, impli), i_rc])

    return func_reac_speed_impli, func_reac_speed_expli


def construct_reac_speed(i_chem, i_reac, impli,
                         stoechio=param_c.Stoechio,
                         Max_reaction_speed=param_c.Max_reaction_speed, K_reaction=param_c.K_reaction,
                         chem=param_c.chem, catalyst=param_c.Catalyst):
    """
    Is called by construct_all_reac_speed_f(). Returns a function reac_speed which computes
    the reaction speed when given a concentration array and a population (of cell) array.
    """

    # select negative stoechiometric coefficents (reactants)
    stoechio_reactants = np.minimum(stoechio[i_reac, :], 0)

    if impli:
        stoechio_numerator = np.abs(stoechio_reactants)
        stoechio_numerator[i_chem] += -1
        stoechio_den = np.abs(stoechio_reactants)

    elif impli == False:
        stoechio_numerator = np.abs(stoechio_reactants)
        stoechio_den = np.abs(stoechio_reactants)

    else:
        raise ValueError("implicit or explicit scheme is not set for chemical ", i_chem,
                         " and reaction ", i_reac)

    product_num = np.empty(shape_conc)
    product_den = np.empty(shape_conc)

    # (Re)create all mutable default parameters before passing them to function at each call of construct_reac_speed_f()
    # to avoid late bindings problems.
    cp_stoechio_numerator = copy.deepcopy(stoechio_numerator)
    cp_stoechio_den = copy.deepcopy(stoechio_den)
    stoechio_main = stoechio[i_reac, i_chem]

    def reac_speed(conc, convol_pop, stoechio_num=cp_stoechio_numerator, stoechio_den=cp_stoechio_den):

        # if catalyst[i_reac] is not None:  # Reaction is catalysed by a cell type

        #     c_count = reac_cell_count(population, catalyst[i_reac])

        # else:
        #     c_count = 1.

        for chm in chem:
            idx = chem_to_int[chm]
            product_num[idx, :] = conc[idx, :]**(stoechio_num[idx])
            product_den[idx, :] = conc[idx, :]**(stoechio_den[idx])

        y = convol_pop*stoechio_main*Max_reaction_speed[i_reac]*np.prod(
            product_num, (0))/(K_reaction[i_reac] + np.prod(product_den, (0)))

        return y

    return reac_speed

###############################################################################
# Some functions to compute the convolution


@ numba.jit(nopython=True)
def interaction(X):
    """
    Could use numpy, but numba does not seem to recognize numpy func. Therefore,
    we need to use numba.
    """
    radius = param.cell_size/2
    Y = np.exp(1 - 1 / (1 - (X/radius)**2))

    return Y


@ numba.jit(nopython=True)  # , parallel=True) #can be parallelized
def sum_interaction(mesh, pos, support, N):
    """
    CAUTION: This function works due to the behaviour of numpy slicing. a[m:n]
    with m > n returns an empty array and not an error. a[0] - np.array([]) returns an empty array
    and not an error.
    """
    out = np.zeros(N)

    for i in range(N):  # prange(N):
        out[i] = np.sum(interaction(
            mesh[i] - pos[support[i, 0]:support[i, 1]]))

    return out


@ numba.jit(nopython=True)
def support_inter_reac(Xa, n, Xb, m, radius):
    # The computation here is more involved than for cell_movement as the vectors Xa and Xb
    # differ in their support.

    out = np.zeros((n, 2), np.int32)
    i_min = 0
    i_max = m - 1

    for i in range(n):

        while (i_min != m) and (Xa[i] - Xb[i_min] > radius - 1e-9):
            i_min = i_min + 1

        out[i, 0] = i_min

    for i in range(n):

        while (i_max != -1) and (Xa[n-1-i] - Xb[i_max] < - radius + 1e-9):
            i_max = i_max - 1

        out[n-1-i, 1] = i_max + 1  # +1 because we want X[i_max] to be included

    return out


def reac_cell_count(population, cell_types, mesh_coord=param_c.coord_mesh_x, c_norm=param.integral_D_half_a):

    # find cells that are of any of the type in cell_types
    try:
        cell_types_int = [param.cell_to_int[i] for i in cell_types]
    except KeyError:
        import pdb
        pdb.set_trace()

    mask1 = [population[:, param.pop_hd['type_c']]
             == i for i in cell_types_int]
    mask = np.any(mask1, axis=0)
    position = population[mask, param.pop_hd['pos']]

    M = position.shape[0]
    N = mesh_coord.shape[0]
    radius = 0.5*param.cell_size

    support = support_inter_reac(mesh_coord, N, position, M, radius)

    Count = sum_interaction(mesh_coord, position, support, N)/c_norm

    return Count
