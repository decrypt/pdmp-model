#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pickle

import parameters as param
import numpy as np

####################################
####### Initialisation #############
####################################


#########################################################
# Main

def init_pop(n_dcs, n_tot, from_distrib, filename=None):
    """
    The position vector that is returned MUST be sorted.
    If from_distrib == True, a file filename is used to sample
    the population.
    """

    if not from_distrib:

        pos_not_dcs = f_position_not_dcs(n_tot - n_dcs)
        pos_dcs = f_position_dcs(n_dcs)

        pos = np.concatenate((pos_dcs, pos_not_dcs))

        # Sort position
        sort = np.argsort(pos)
        pos = pos[sort]

        idx_sort = np.argsort(sort)
        idx_dcs = idx_sort[:n_dcs]  # get position of dcs cells

        kind = f_kind(idx_dcs)

        population = np.empty((n_tot, param.n_cell_var))
        population[:, param.pop_hd['id']] = np.arange(
            0, n_tot)  # set cell numbers STARTING AT ZERO
        population[:, param.pop_hd['type_c']] = kind  # set cell kinds
        population[:, param.pop_hd['pos']] = pos  # set position

    elif from_distrib:
        Pop = pickle.load(open(filename, 'rb'))
        Pop_pos = sample_initial_pop(Pop)

        # Sample DCS cells from a better distribution:
        if 'dcs' in Pop_pos.keys():
            Pop_pos['dcs'] = f_position_dcs(n_dcs)

        # Get position of cells as a numpy array
        pos = np.array([])
        for cell in Pop_pos.keys():
            pos = np.concatenate((pos, Pop_pos[cell]))

        # Get type of cells as a np.array
        type = []
        for cell in Pop_pos.keys():
            type += [param.cell_to_int[cell]]*len(Pop_pos[cell])
        type = np.array(type)

        # Sort positions
        arg_sort = np.argsort(pos)
        pos = pos[arg_sort]
        type = type[arg_sort]

        # Construct population array
        population = np.empty((len(pos), param.n_cell_var))
        population[:, param.pop_hd['pos']] = pos  # position
        population[:, param.pop_hd['type_c']] = type  # type
        population[:, param.pop_hd['id']] = np.arange(0, len(pos))  # id

    else:
        raise ValueError('Argument from_distrib is boolean.')

    return population


################################
# Positions

def f_position_dcs(n):
    z_sc = 12.  # limit of stem cell niche

    # Probability density of dcs
    # d(z) = 2/z_sc * (1 -  z/z_sc)
    # Inverse:
    def inverse(u): return z_sc*(1 - np.sqrt(1 - u))

    U = np.random.random_sample(n)
    pos = inverse(U)
    return pos


##########################################################
# If from_distrib = False
##########################################################

################################
# Positions

def f_position_not_dcs(n, opt=None):
    # Each posiiton is drawn randomly.

    Position = np.random.random_sample(n)*param.z_max

    if opt == 'dcs_bonus':
        # Add 10 dcs in 20 first mu m.
        Position_dcs = np.random.random_sample(15)*20
        Position = np.concatenate((Position, Position_dcs))

    return Position


##################################
# Kinds

def f_kind(idx_dcs, opt=None):
    # Bottom
    # Mixed sc and dcs:
    n_sc = 16
    Kind_bot = [param.cell_to_int['sc']]*n_sc

    n_pc = 189
    Kind_mid = [param.cell_to_int['pc']]*n_pc

    # Mixed gc and ent
    n_gc = 125
    n_ent = 344
    # Draw n_gc numbers to be goblet cells
    l = [i for i in range(n_gc + n_ent)]
    Kind_top = [param.cell_to_int['ent']]*(n_gc + n_ent)
    for i in range(n_gc):
        tmp = np.random.random(1)*len(l)
        idx = l[int(tmp)]
        Kind_top[idx] = param.cell_to_int['gc']
        l.pop(int(tmp))

    Kind = Kind_bot + Kind_mid + Kind_top

    idx_dcs = np.sort(idx_dcs)
    for idx in idx_dcs:
        Kind.insert(idx, param.cell_to_int['dcs'])

    Kind = np.asarray(Kind)

    return Kind

##########################################################
# If from_distrib = True
##########################################################


def sample_distrib(points, cumul, n):
    u = np.random.rand(n)
    x = np.interp(u, cumul, points)
    return x


def sample_initial_pop(Pop):
    Total_cell = {}
    Cumul_distrib_cell = {}
    Pop_pos = {}
    edges = np.linspace(0, param.z_max, num=len(Pop[list(Pop.keys())[0]]) + 1)
    width = edges[1] - edges[0]

    for cell in Pop:
        Total_cell[cell] = np.floor(np.sum(Pop[cell]) * width)
        Cumul_distrib_cell[cell] = np.cumsum(Pop[cell]) / np.sum(Pop[cell])
        Cumul_distrib_cell[cell] = np.insert(Cumul_distrib_cell[cell], 0, 0.)
        Pop_pos[cell] = sample_distrib(
            edges, Cumul_distrib_cell[cell], np.int(Total_cell[cell]))

    return Pop_pos
