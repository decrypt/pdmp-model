#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
from scipy import integrate as scp_integrate
from scipy.sparse import csr_matrix

import parameters as param
import parameters_chem as param_c  # to get the space coordinates of mesh points


pop_hd = param.pop_hd

#############################
##### Jumps rates #######
#############################


rates_max = {}
K = {}
l = {}
for i in param.cell_types:
    rates_max[i] = {}
    K[i] = {}
    l[i] = {}

##############################
# Max rates: MUST define at least one per cell type
# Stem cell
rates_max['sc']['sc'] = 0.15  # 0.09
rates_max['sc']['pc'] = 0.2  # 0.04

# Progenitor cell
rates_max['pc']['pc'] = 0.22
rates_max['pc']['ent'] = 0.25  # 0.15
rates_max['pc']['gc'] = rates_max['pc']['ent'] * 0.33

# Goblet cell
rates_max['gc']['ex'] = 0.34  # 0.4

# Enterocyte
rates_max['ent']['ex'] = 0.34  # 0.4

# DCS
rates_max['dcs']['dcs'] = 0.


# Michaelis constant
K['ci'] = 41.

K['sc']['sc_z'] = 12.
K['sc']['sc_but'] = 2.
K['sc']['ci'] = 53.

K['pc']['pc_z'] = 40.

K['pc']['ent_z'] = 40.
K['pc']['ent_but'] = 1.5

K['ent']['ex_z'] = 190.
K['ent']['ex_ci'] = 20.


# l (width of transition in regulation function)
l['ci'] = 6.
l['sc']['sc_z'] = 5.
l['sc']['sc_but'] = 5.

l['pc']['pc_z'] = 40.

l['pc']['ent_z'] = 15.
l['pc']['ent_but'] = 5.

l['ent']['ex_z'] = 15.


###############################################################################
# Main


def f_rates(cell, population, concentration, arg_K, b_CI):
    """
    ci : expected value is parameters.contact_inhibtion . For on/off contact inhibition.


    Returns a np.array Q of size sizepop*number_jump_types 
    where Q[i,j] is the jump rates of cell i for jump type j.
    """

    # Parsed arguments
    if 'ci' in arg_K:
        K['ci'] = arg_K['ci']

    # return a matrix of size (sizepop * number of type of jumps) where each coefficient is the rate
    # of that particular jump
    Q = np.zeros(len(param.jump_types))

    ######################
    # Compute local density

    pos_cell = cell[param.pop_hd['pos']]
    density = eval_density(pos_cell, population[:, param.pop_hd['pos']])

    #########################################################
    # Set inhibition of proliferation by CONTACT INHIBITION if
    # option is activated.
    ci_effect = {}
    if b_CI == False:  # no inhibition
        ci_effect['act'] = lambda x, K, n: 1.  # inhibition
        ci_effect['inh'] = lambda x, K, n: 1.  # activation

    elif b_CI == True:  # inhibition: so far the effect is the same for each type
        # of jump

        ci_effect['act'] = Pol3  # activation
        ci_effect['inh'] = lambda x, K, n: 1 - Pol3(x, K, n)  # inhibition

    #######################################################################
    #######################################################################
    # Compute jump rate of cell

    type_c = param.int_to_cell[cell[param.pop_hd['type_c']]]

   #############
    # Stem cell
    #############
    if type_c == 'sc':

        idx_buty = param_c.chem_to_int["buty"]
        buty = concentration[idx_buty][:-1]
        buty_at_cell = eval_concentration_at_cell(pos_cell, buty)

        ####################
        # Rate to stem cell:

        r_max = rates_max['sc']['sc']
        rates = r_max * ci_effect['inh'](density, K['sc']['ci'], l['ci']) *\
            (1 - Pol3(pos_cell, K['sc']['sc_z'], l['sc']['sc_z'])) *\
            (1 - Pol3(buty_at_cell, K['sc']['sc_but'], l['sc']['sc_but']))

        Q[param.jump_to_int['sc']] = rates

        ##########################
        # Rate to progenitor cell:

        r_max = rates_max['sc']['pc']

        rates = r_max * Pol3(pos_cell, K['sc']['sc_z'], l['sc']['sc_z'])

        Q[param.jump_to_int['pc']] = rates

    #################
    # Progenitor cell
    #################
    elif type_c == 'pc':

        idx_buty = param_c.chem_to_int["buty"]
        buty = concentration[idx_buty][:-1]
        buty_at_cell = eval_concentration_at_cell(pos_cell, buty)

        ############################
        # To progenitor cell:

        r_max = rates_max['pc']['pc']

        rates = r_max * ci_effect['inh'](density, K['ci'], l['ci']) *\
            (1 - Pol3(pos_cell, K['pc']['pc_z'], l['pc']['pc_z']))

        Q[param.jump_to_int['pc']] = rates

        ##########################
        # To goblet cell

        r_max = rates_max['pc']['gc']

        rates = r_max * Pol3(pos_cell, K['pc']['ent_z'], l['pc']['ent_z'])\
            * Pol3(buty_at_cell, K['pc']['ent_but'], l['pc']['ent_but'])

        # print(rates/r_max)

        Q[param.jump_to_int['gc']] = rates

        ###########################
        # To enterocytes

        r_max = rates_max['pc']['ent']

        rates = r_max * Pol3(pos_cell, K['pc']['ent_z'], l['pc']['ent_z']) \
            * Pol3(buty_at_cell, K['pc']['ent_but'], l['pc']['ent_but'])

        Q[param.jump_to_int['ent']] = rates

    #############
    # Goblet cell
    #############
    elif type_c == 'gc':

        # Extrusion

        r_max = rates_max['gc']['ex']

        rates = r_max * ci_effect['act'](density, K['ent']['ex_ci'], l['ci']) *\
            Pol3(pos_cell, K['ent']['ex_z'], l['ent']['ex_z'])

        Q[param.jump_to_int['ex']] = rates

    #############
    # Enterocytes
    #############

    elif type_c == 'ent':

        # Extrusion

        r_max = rates_max['ent']['ex']

        rates = r_max * ci_effect['act'](density, K['ent']['ex_ci'], l['ci']) *\
            Pol3(pos_cell, K['ent']['ex_z'], l['ent']['ex_z'])

        Q[param.jump_to_int['ex']] = rates

    return Q


# Return maximal rates
def f_rates_max(population):
    """
    Return vector of maximal rates for each cell.
    """
    out = np.empty(population.shape[0])
    for i in param.cell_types:
        # upper boundary on the total jump rate of a given cell kind
        r_max_type = sum(rates_max[i].values())
        mask = population[:, param.pop_hd['type_c']] == param.cell_to_int[i]
        out[mask] = r_max_type

    return out


###############################################################################
# Regulation functions

def Hill(x, K, n):

    y = x**n/(K**n + x**n)

    return y


def Pol3(x, K, l):

    a = -1 / (4*l**3)
    b = (3*K)/(4*l**3)
    c = -(3*K**2 - 3*l**2) / (4*l**3)
    d = ((K**3+2*l**3 - 3*K*l**2))/(4*l**3)

    # if x is a vector
    if hasattr(x, '__len__'):
        Y = np.zeros(x.shape)
        mask = (x > K - l) & (x < K + l)
        Y[mask] = a*x[mask]**3 + b*x[mask]**2 + c*x[mask] + d

        mask = x >= K + l
        Y[mask] = 1.

    else:
        if x < (K-l):
            Y = 0.
        elif x < (K + l):
            Y = a*x**3 + b*x**2 + c*x + d
        else:
            Y = 1.

    return Y


###############################################################################
# Compute convolution terms


def eval_density(pos_cell, pos_pop):
    """
    Compute the convolution of the positions vector with the density kernel, at 
    each position of the positions vector.
    """
    # Parameters
    cell_size = param.cell_size
    scaling = param.scaling
    z_max = param.z_max
    step = param.step_density
    correction_table = param.correction_table

    dens = 0.

    # find cells which at a distance less than cell_size of pos_cell
    i_min = np.searchsorted(pos_pop - pos_cell, -cell_size, side='right')
    i_max = np.searchsorted(pos_pop - pos_cell, cell_size, side='left')

    if i_min - i_max != 0:
        Distance = pos_cell - pos_pop[i_min:i_max]
        Dens = np.exp(1 - 1 / (1e-13 + 1 - (Distance/cell_size)**2))
        dens = np.sum(Dens)/scaling

    # Correct for boundary effects on density
    if pos_cell < cell_size:
        idx = int(pos_cell // step)
        correction = correction_table[idx]
        dens = dens*correction

    elif z_max - pos_cell < cell_size:
        idx = int((z_max - pos_cell) // step)
        correction = correction_table[idx]
        dens = dens*correction

    return dens


def mat_convol_conc(z_coord=param_c.coord_mesh_x):

    centers = (z_coord[1:] + z_coord[:-1])/2
    mat = np.zeros((z_coord.shape[0] - 1, z_coord.shape[0] - 1))

    # Kernel
    radius = param.cell_size/2
    def D_half_a(z): return np.exp(1 - 1 / (1 - (z/radius)**2))

    # Construct vector of convolution for grid cells not at the boundary
    # find first grid cell whose center is
    idxbot = np.searchsorted(centers - z_coord[0], radius + 1e-13)
    # at a greater distance than radius
    # from lower boundary
    z = centers[idxbot]
    idx_integral = np.asarray(np.logical_and(
        z_coord > z - radius, z_coord < z + radius).nonzero()[0])
    integral_bounds = z_coord[idx_integral]
    conv = np.zeros(integral_bounds.shape[0] - 1)

    for j, (z0, z1) in enumerate(zip(integral_bounds[:-1], integral_bounds[1:])):
        conv[j] = scp_integrate.quad(D_half_a, z-z1, z-z0)[0]

    # Integrate over grid cells that are not completely included in the support of
    # D_half_a
    # Lower grid cell
    zmin = np.asarray([max(z_coord[0], z - radius + 1e-13), ])
    if zmin == integral_bounds[0]:
        pass
    else:
        tmp = scp_integrate.quad(D_half_a, z-integral_bounds[0], z-zmin)[0]
        conv = np.insert(conv, 0, tmp)

    # Upper grid cell
    zmax = np.asarray([min(z_coord[-1], z + radius - 1e-13), ])
    if zmax == integral_bounds[-1]:
        pass
    else:
        tmp = scp_integrate.quad(D_half_a, z-zmax, z-integral_bounds[-1])[0]
        conv = np.append(conv, tmp)

    mat = np.zeros((centers.shape[0], centers.shape[0]))
    for i_x, x in enumerate(conv):
        mat += x*np.eye(centers.shape[0], k=i_x - idxbot)

    return csr_matrix(mat)


Convol_conc = mat_convol_conc()
# print(Convol_conc)


def eval_concentration_at_cell(pos, concentration, z_coord=param_c.coord_mesh_x,
                               Mat_convol=Convol_conc, Cst=param.integral_D_half_a):
    """
    Return the convolution of concentration taken at point pos.
    """

    step = z_coord[1] - z_coord[0]

    idx_z = int((pos - z_coord[0]) // step)

    conc_at_cell = Mat_convol[idx_z].dot(concentration)

    # print(conc_at_cell)

    conc_at_cell = conc_at_cell/Cst

    # print('Divided',conc_at_cell)

    return conc_at_cell
