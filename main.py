#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug  7 11:58:35 2019

@author: ldarrigade
"""


# ******************************************************************************

# **********
# MUST READ:
# **********
#
# All computations of convolutions use the fact that cell positions are sorted.
# This order therefore must be maintained When positions are sorted:
#    - at initialisation: init.init_pop() returns a vector of sorted positions
#
#    - after each time step of displacement of cells
#
#    - at birth of a new cell, which is inserted in the population array while keeping
#    its order.

# ******************************************************************************


import time
import argparse
import numba
from scipy.sparse import csr_matrix
from scipy.linalg import inv, solveh_banded
import numpy as np


# try:
#     numba.set_num_threads(1)

# except AttributeError:
#     pass

import reaction
import diffusion as diff
import parameters_chem as param_c
import cell_movement
import gillepsie as gil
import init
import parameters as param


def main(**kwargs_main):
    """
    kwargs_main: optionnal to allow for a call to main without it.
    """

    #####################################################################
    # Parsed arguments (default value are shown at the end of the script)
    # Beware, arguments that are not specified are still parsed as None.

    # Parsed arguments with default value (see end of script)
    b_CI = kwargs_main['contact_inhibition']
    simu_path = kwargs_main['simu_path']
    param.scaling = kwargs_main['scaling']
    param.k_spring = kwargs_main['spring']

    # Arguments for jump_rates.py
    arg_K = {}
    if kwargs_main['K_ci'] is not None:
        arg_K['ci'] = kwargs_main['K_ci']

    ###################
    ### Simulation ####
    ###################

    start = time.time()

    # To keep track of the execution
    Jump = 0
    Jump_val = 0

    ###############################################################################
    # Prepare snapshot
    n_snap = 200
    t_snap = np.linspace(0, param.T_simu, n_snap)
    count_snap = 0

    #############################
    # Create array to save results
    #############################

    # Arrays describing population
    # Array for storing successive state of the population.
    # Number of rows is the number of cell variables (see parameters).
    pop_hd = param.pop_hd  # headers for R_pop
    R_pop = []
    daughter = np.empty(param.n_cell_var)

    # Array for storing current time everytime the state of the population is saved in
    # R_pop (used in post-processing).
    R_time = []

    # Array for storing current size of the population everytime the state of the population is saved in
    # R_pop (used in post-processing).
    R_sizepop = []

    # Array storing jumps. Number of rows and content is defined in jump_variables
    # (see parameters).
    jmp_hd = param.jmp_hd  # headers for R_jump
    R_jump = []  # save at each random jump the time, number of mother cell and kind, number of daughter
    # cell and kind (if applicable)
    jump = np.empty(param.n_jmp_var)

    R_conc = []

    ####################################
    ###### Initialization (T=0)  #######
    ####################################

    ##############################################################################
    # Some precomputation
    # done now to take into account possible parsed parameters
    param.correction_table, param.step_density = param.f_correction_table()

    t = 0
    ###############################################################################
    # Initialize population: np.array of 3 column (id_cell, type_c and position)

    # ,opt = 'dcs_bonus')
    population = init.init_pop(
        param.n_dcs_init, param.n_tot_init, from_distrib=True, filename='Data_pop_init.p')

    # a counter to assign a new number to each cell newly created through division
    counter_cell = max(population[:, 0])

    ################################################################################
    # Initialize reaction-diffusion

    n_chem = param_c.n_chem
    n_mesh_x_rd = param_c.n_mesh_x  # rd for reaction diffusion
    step_t_rd = param.step_t_rd
    step_t_cellmov = param.step_t_cellmov

    # Diffusion
    operator_diff, f_term, BC_values, BC_dep_time, \
        LHS_dep_time, RHS_dep_time = diff.init_diffusion()

    # As for the main part of diffusion steps, the time step is not changed, the
    # diffusion matrix does not change neither: it can be pre-inversed. This
    # inverse will be directly used if step = step_t_rd.

    operator_inv = {}
    for chem in param_c.chem:
        idx_chm = param_c.chem_to_int[chem]
        operator_inv[idx_chm] = csr_matrix(inv(diff.diffusion_matrix(
            BC_values[chem], step_t_rd)))

    # Reaction
    func_reac_speed_impli, func_reac_speed_expli = reaction.init_reaction()

    ###############################################################################
    # Initialize concentration

    # initialise concentration: a matrix n_chem*n_mesh_x
    Conc = np.empty((n_chem, n_mesh_x_rd))

    Conc[param_c.chem_to_int['o2'],
         :], Conc[param_c.chem_to_int['buty'], :] = param_c.init_conc()

    ###############################################################################
    # Save intial state

    R_conc.append(np.copy(Conc))

    R_pop.append(np.copy(population))
    R_time.append(t)
    R_sizepop.append(population.shape[0])
    count_snap += 1

    ###############################################################################
    # Some functions

    @numba.jit(nopython=True)
    def get_moving(X, types_moving=param.types_moving):
        """ The fastest way would be to do a list comparison. But here, cell types are stored
        a numpy.array. Therefore, we turn to Numba which performs as fast.
        Args:
            - X : a numpy array (of float or int)
            - types_moving : np.array of types (int) of moving cells
        """
        mask = np.zeros(X.shape[0], dtype=np.bool_)

        for i in range(X.shape[0]):
            for type_c in types_moving:
                if X[i] == type_c:
                    mask[i] = 1
                    break
        return mask

    ###############################################################################
    # Definition of iteration functions

    def iteration_rd(step, t, convol_pop_reac, X0, step_t_rd=step_t_rd):
        """
        Simulate reaction-diffusion.
        """
        # Operator splitting: first simulate a diffusion step for all chemical and the reaction step.

        X1 = np.empty(X0.shape)
        X_half = np.empty(X0.shape)

        # ATTENTION:: Update time as terms in the boundary condition can depend of time
        t_loc = t + step

        ######################################################################
        # Reaction step

        for chemical in param_c.chem:
            idx_chm = param_c.chem_to_int[chemical]

            R_impli = np.zeros(n_mesh_x_rd)
            R_expli = np.zeros(n_mesh_x_rd)

            # Note that for each for-loop, if the list is empty, nothing happens and it does not
            # raise an error
            for idx_reac in range(len(func_reac_speed_impli[idx_chm])):

                k_reac = func_reac_speed_impli[idx_chm][idx_reac][1]
                R_impli += func_reac_speed_impli[idx_chm][idx_reac][0](
                    X0, convol_pop_reac[k_reac, :])

            for idx_reac in range(len(func_reac_speed_expli[idx_chm])):

                k_reac = func_reac_speed_expli[idx_chm][idx_reac][1]
                R_expli += func_reac_speed_expli[idx_chm][idx_reac][0](
                    X0, convol_pop_reac[k_reac, :])

            operator_impli_reac = 1/(np.ones(n_mesh_x_rd) - step*R_impli)
            X_half[idx_chm] = (X0[idx_chm] + step*R_expli)*operator_impli_reac

        ######################################################################
        # Diffusion step

        for chem in param_c.chem:
            idx_chm = param_c.chem_to_int[chem]

            ########################################
            # Check for time dependency
            if (LHS_dep_time[chem] is True) or (RHS_dep_time[chem] is True):
                # evaluate BC_values
                diff.evaluate_BC_values_at_t(
                    BC_values, chem, t_loc, BC_dep_time=BC_dep_time)

            if RHS_dep_time[chem] is True:
                f_term[idx_chm] = diff.set_f_term_in_RHS(BC_values[chem])

            ##############################################
            # Compute the new state
            RHS = diff.func_RHS(
                X_half[idx_chm], f_term[idx_chm], BC_values[chem], step)

            if step == step_t_rd:
                X1[idx_chm] = operator_inv[idx_chm].dot(RHS)
            else:
                operator_diff[idx_chm] = diff.diffusion_matrix(
                    BC_values[chem], step)
                A = operator_diff[idx_chm]
                A_banded = np.zeros((2, A.shape[0]))
                A_banded[0, :] = np.diag(A)
                A_banded[1, :-1] = np.diag(A, k=1)

                X1[idx_chm] = solveh_banded(A_banded, RHS, lower=True)

        return X1

    def iteration(step, t, population, Conc, step_t_rd=step_t_rd):
        # The time step necessary for reaction-diffusion (param.step_t_rd) is much smaller than the time step
        # for movement of cells (param.step_t_cellmov). Therefore, we run first
        # a great number of small reaction-diffusion step, and then one big step
        # of cell movement.

        ###########################################################################
        # Reaction-diffusion
        ###################################

        # We compute reaction terms involving cell once for all the small time steps (of size
        # param.step_t_rd), as cells are constant during this period.

        convol_pop_reac = np.ones((len(param_c.reac), n_mesh_x_rd))
        for i_reac in param_c.reac_to_int.values():

            # Reaction is catalysed by a cell type
            if param_c.Catalyst[i_reac] is not None:
                convol_pop_reac[i_reac, :] = reaction.reac_cell_count(
                    population, param_c.Catalyst[i_reac])

        step_remaining = step
        while step_remaining - step_t_rd >= 0:
            Conc = iteration_rd(step_t_rd, t, convol_pop_reac,
                                Conc, step_t_rd=step_t_rd)
            t += step_t_rd
            step_remaining += - step_t_rd

        if step_remaining != 0:
            Conc = iteration_rd(step_remaining, t, convol_pop_reac,
                                Conc, step_t_rd=step_t_rd)
            t += step_t_rd

        ###########################################################################
        # Movement of cells
        ######################################

        # WARNING: assume cells position are sorted
        mask_moving = get_moving(population[:, pop_hd['type_c']])

        # If some cells are moving
        if np.sum(mask_moving) > 0:

            speed = cell_movement.force_repulsive(
                population[:, pop_hd['pos']], mask_moving)

            population[mask_moving, pop_hd['pos']] += step*speed

            # Bring back cells that have crossed boundary
            mask_top = population[:, pop_hd['pos']] > param.z_max
            population[mask_top, pop_hd['pos']] = param.z_max

            mask_bot = population[:, pop_hd['pos']] < 0.
            population[mask_bot, pop_hd['pos']] = 0.

        # Sort population by position
        argsort_position = np.argsort(population[:, pop_hd['pos']])
        population = population[argsort_position]

        return population, Conc

    ##############################################################################

    ###################
    ###### Run  #######
    ###################

    jump_boo = True
    next_print = param.T_simu / 20
    curr_time = time.time()
    while t < param.T_simu and population.shape[0] != 0 and population.shape[0] < param.sizepop_max and count_snap < n_snap:

        if t > next_print:
            now = time.time()
            iteration_time = now-curr_time
            curr_time = now
            print('time :', t, '/', param.T_simu,
                  '. Last iterations time', iteration_time)
            next_print += 1

        ##########################
        #Selection of a jump time#
        ##########################

        # t_jump is the time interval until next jump
        t_jump = gil.Gillepsie_jump_time(
            population, arg_K, b_CI=b_CI)

        Jump += 1

        ############################
        ### Determinist evolution###
        ############################

        if t + t_jump > param.T_simu:
            t_jump = param.T_simu - t
            jump_boo = False  # this variable is false if the proposed jump time is beyond the end of the
            # simulation param.T_simu.

        while t_jump > 0:

            actual_step = min(step_t_cellmov, t_jump)

            # ATTENTION: note that time is updated after call to iteration()
            population, Conc = iteration(
                actual_step, t, population, Conc)
            t += actual_step
            t_jump += -actual_step  # remaining duration before reaching jump time
            if t < t_snap[count_snap]:
                pass

            elif t >= t_snap[count_snap]:

                ###########################
                ########## Save ###########

                # Save time
                R_time.append(t)

                # Save concentrations
                R_conc.append(np.copy(Conc))

                # Save state of population
                R_sizepop.append(population.shape[0])
                R_pop.append(np.copy(population))

                # Update snap counter
                count_snap += 1

        ############
        ### Jump ###
        ############

        # Selection of a jump type and modification of the population if the jump
        # is accepted.

        mother_line, jump_type = gil.Gillepsie_jump_type(
            population, Conc, arg_K, b_CI=b_CI)

        # we modify the population if the jump has been accepted
        if (jump_boo) & (not np.isnan(mother_line)):

            Jump_val += 1

            mother = np.copy(population[mother_line, :])
            daughter_id = np.nan  # to modify if the jump is a division
            # and save in R_jump array

            if jump_type == "ex":  # extrusion
                population = np.delete(population, mother_line, axis=0)

            # differenciation
            elif param.jump_to_int[jump_type] != mother[pop_hd['type_c']]:
                population[mother_line, pop_hd['type_c']
                           ] = param.cell_to_int[jump_type]

            else:  # division: we add a cell at the end of population array
                # daughter = np.empty(param.n_cell_var) was called at the beginning of amin
                counter_cell += 1
                daughter_id = counter_cell  # saved in R_jump array
                daughter[pop_hd['id']] = counter_cell
                daughter[pop_hd['type_c']] = mother[pop_hd['type_c']]
                daughter[pop_hd['pos']] = mother[pop_hd['pos']] + \
                    param.shift_at_division(mother[pop_hd['pos']])

                # Insert cell to maintain the sorted order of population array.
                row_insert = np.searchsorted(
                    population[:, pop_hd['pos']], daughter[pop_hd['pos']])
                population = np.insert(
                    population, row_insert, daughter, axis=0)

            # Save informations about the jump
            # jump = np.empty(param.n_evt_var) was called at the beginning of main
            jump[jmp_hd['t']] = t
            jump[jmp_hd['id_mom']] = mother[pop_hd['id']]
            jump[jmp_hd['type_mom']] = mother[pop_hd['type_c']]
            jump[jmp_hd['pos_mom']] = mother[pop_hd['pos']]
            jump[jmp_hd['type_jump']] = param.jump_to_int[jump_type]
            jump[jmp_hd['id_daug']] = daughter_id
            jump[jmp_hd['pos_daug']] = daughter[pop_hd['pos']]

            R_jump.append(np.copy(jump))

        else:  # do nothing if the jump is rejected
            pass

    ###############################################################################
    # End of simulation

    if t >= param.T_simu:
        print("This is the end my friend, final time is {} hours.\n".format(
            t))
        print(f'Size of the population: {R_sizepop[-1]}')

    elif population.shape[0] >= param.sizepop_max:
        print("The population is exploding, sizepop = {}\n".format(
            population.shape[0]))

    elif population.shape[0] == 0:
        print("There are no more cells, sizepop = {}\n".format(
            population.shape[0]))

    final_t = time.time()
    print('Total runtime: %.1f sec\n' % (final_t - start))

    # Save R_pop in a numpy.array R_pop_clean where R_pop[i] are stored contiguously
    R_pop_clean = np.vstack(R_pop)
    R_time_clean = np.array(R_time)
    R_sizepop_clean = np.array(R_sizepop)

    if len(R_jump) != 0:
        R_jump_clean = np.vstack(R_jump)

    elif len(R_jump) == 0:  # no jump happend
        fake_jump = np.empty(param.n_jmp_var)
        fake_jump[jmp_hd['t']] = t + param.T_simu
        fake_jump[jmp_hd['id_mom']] = counter_cell + 100
        fake_jump[jmp_hd['type_mom']] = 0.
        fake_jump[jmp_hd['pos_mom']] = 2*param.z_max
        fake_jump[jmp_hd['type_jump']] = 0.
        fake_jump[jmp_hd['id_daug']] = counter_cell + 100
        fake_jump[jmp_hd['pos_daug']] = 2*param.z_max
        R_jump_clean = fake_jump.reshape(1, param.n_jmp_var)

    R_conc_clean = np.asarray(R_conc)

    ##########################################
    # Save results
    np.savez(simu_path, pop=R_pop_clean, time=R_time_clean,
             sizepop=R_sizepop_clean, jump=R_jump_clean, conc=R_conc_clean)

    # Return value
    # pass result arrays (R_._clean) to a dictionnary to match visualisation
    result = {}
    # requirements of data format
    result['pop'] = R_pop_clean
    result['time'] = R_time_clean
    result['sizepop'] = R_sizepop_clean
    result['jump'] = R_jump_clean

    return result


##################################################################################################
##################################################################################################

if __name__ == "__main__":

    ############################################################################
    # Parsed options for simulation
    parser = argparse.ArgumentParser(
        description='To run DeCrypt', formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('-ci', '--contact_inhibition', type=str, choices=[True, False],
                        default=True, help='Activate or no  contact inhibition.')

    parser.add_argument('-N', '--scaling', type=float, default=1.,
                        help='Scaling parameter which determines the weight of each cell.')

    parser.add_argument('-p', '--simu_path', type=str, default='simu/simu',
                        help='Path to the file where simulation results are saved.')

    parser.add_argument('-k', '--spring', type=float, default=15.,
                        help='Value of the spring constant ($k_a$ in Darrigade et al., A PDMP model of the epithelial cell turn-over in the intestinal crypt including microbiota-derived regulations. (2021).).')

    parser.add_argument('-Kci', '--K_ci', type=float,
                        help='Parameter for intensity of contact inhibition($K_{div, sc}[dens]$ in Darrigade et al., A PDMP model of the epithelial cell turn-over in the intestinal crypt including microbiota-derived regulations. (2021).).')

    parsed_args = parser.parse_args()

    kwargs_main = {}
    for args in vars(parsed_args):
        kwargs_main[args] = getattr(parsed_args, args)

    result = main(**kwargs_main) or (None, None)
