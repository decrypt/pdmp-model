#!/usr/bin/env python3
# -*- coding: utf-8 -*-


# ******************************************************************************

# We use an implicit scheme.

# Boundary conditions are written as follows:
#
# - Dirichlet condition: c = f
#
# - Robin condition: a * d_z c + b*c = f
#
# - Neumann condition: grad(c) \scal n = f,
#    where n is the normal unit vector, pointing
#     outward of the domain. Therefore, a positive f will always be a positive incoming flux, no matter where on the
#    boundary we are situated.
#
#
# Call init_diffusion() and return all the object that will be used to simulate
# diffusion.

# ******************************************************************************

# to limit the number of threads used by numpy and scipy
import parameters as param
import parameters_chem as param_c
import copy  # to construct ditionnary BC_indep_time from param_c.BC
import inspect  # to check whether coefficient are time or space dependent
import numpy as np
from scipy.linalg import inv

diff_coeff = param_c.diff_coeff
n_chem = param_c.n_chem
n_mesh_x = param_c.n_mesh_x
step_t = param.step_t_rd
step_x = param_c.step_x


###############################################################################
# Main function

def init_diffusion():
    """Initialise diffusion objects."""

    # dictionnary which has the same nested structure as param_c.BC
    BC_values = copy.deepcopy(param_c.BC)

    # Check for time dependency of boundary coefficients: construct a nested dict
    # which stores name of variables depending of t.
    BC_dep_time = {}
    for chem in param_c.BC:
        BC_dep_time[chem] = {}
        for loc in param_c.BC[chem]:
            BC_dep_time[chem][loc] = []
            for key in param_c.BC[chem][loc]:

                try:
                    full_arg = inspect.getfullargspec(
                        param_c.BC[chem][loc][key])
                # no argument so no dependence in time (float eg)
                except TypeError:
                    pass

                else:  # has arguments
                    if 't' in full_arg.args:  # check if t is one of the arguments
                        BC_dep_time[chem][loc].append(key)

                    else:  # t is not one of the arguments
                        pass

    LHS_dep_time = {}
    RHS_dep_time = {}

    for chem in param_c.chem:
        idx_chm = param_c.chem_to_int[chem]
        LHS_dep_time[chem] = True

#        ##If diffusion matrix is independent of time, it is constucted once and for all
#        if (
#            'a' in BC_dep_time[chem]['bas'] or 'b' in BC_dep_time[chem]['bas'] or
#            'a' in BC_dep_time[chem]['lum'] or 'b' in BC_dep_time[chem]['lum']
#                ):
#
#            diff_inde_of_time[chem] = 'no'
#
#        if diff_inde_of_time[chem] == 'yes':
#            #no need to evaluate BC_values at t
#            operator_diff[idx_chm] = diffusion_matrix(chem,BC_values[chem])

        if ('f' in BC_dep_time[chem]['bas'] or 'f' in BC_dep_time[chem]['lum']):

            RHS_dep_time[chem] = True

        else:
            RHS_dep_time[chem] = False

    # Construct diffusion operators if they are independent of time
    operator_diff = np.empty((n_chem,)+(n_mesh_x, n_mesh_x))
    f_term = np.empty((n_chem, n_mesh_x))

    for idx_chem, chem in enumerate(param_c.chem):
        if RHS_dep_time[chem] == False:
            f_term[idx_chem] = set_f_term_in_RHS(BC_values[chem])

    return operator_diff, f_term, BC_values, BC_dep_time, LHS_dep_time, RHS_dep_time


###############################################################################
# Construct diffusion matrix

def generic_diff_mat():
    """Return generic diffusion matrix."""

    # Construct the generic diffusion matrix  using an implicit scheme.
    diag_0 = 2*np.ones(n_mesh_x)
    diag_m1 = -1 * np.ones(n_mesh_x - 1)
    diag_p1 = np.copy(diag_m1)
    diff_mat = np.diag(diag_0) + np.diag(diag_m1, -1) + np.diag(diag_p1, 1)
    diff_mat = (diff_coeff/step_x**2)*diff_mat
    # preconditioner = inv(diff_mat)
    return diff_mat


Cst_diff_mat = generic_diff_mat()


def diffusion_matrix(BC_val, step_t, Cst_diff_mat=Cst_diff_mat):
    """Return inverted diffusion matrix adapted to the specific boundary
    condition.
    Cst_diff_mat is defined outside of the function, so that it is not constructed
    at each iteration."""

    # take into account boundary condition
    diff_mat = set_BC_in_diff_mat(Cst_diff_mat, BC_val)
    # before inversion of the matrix (not for Dirichlet). Also, raise an
    # error if the BC is unknown.

    diff_mat = np.identity(n_mesh_x) + step_t*diff_mat

    return diff_mat


def set_BC_in_diff_mat(diff_mat, BC_val):
    """Modify diffusion matrix to take into account boundary condition
    before inversion."""

    for position in BC_val:  # loop over basal and luminal BC
        vect = np.zeros(n_mesh_x)

        if position == 'bas':
            i = 0  # line of diff_mat to work on
            j0 = 0  # columns of diff_mat to work on
            j1 = 1

        elif position == 'lum':
            i = -1
            j0 = -1
            j1 = -2

        else:
            raise ValueError("BC position key (%s) is unknown" % position)

        ##################
        # Neumann condition
        if BC_val[position]['cat'] == 'N':
            # Modify coefficient of u^n_1 coming from u^n_{-1}.
            vect[j0] = (diff_coeff/step_x**2)
            vect[j1] = -(diff_coeff/step_x**2)

        #################
        # Robin condition
        elif BC_val[position]['cat'] == 'R':
            a = BC_val[position]['a']
            vect[j0] = (diff_coeff/step_x**2)*(1 + step_x*a)
            vect[j1] = -(diff_coeff/step_x**2)

        #################
        # Dirichlet condition
        elif BC_val[position]['cat'] == 'D':
            vect[j0] = 2*(diff_coeff/step_x**2)
            vect[j1] = -(diff_coeff/step_x**2)

        else:
            raise ValueError('BC cat (%s) is unknown' %
                             BC_val[position]['cat'])

        diff_mat[i, :] = np.copy(vect)

    return diff_mat


###############################################################################
# Construct right handside

def func_RHS(X0, f_term, BC_chem, step):

    RHS = np.copy(X0)

    for position in BC_chem:

        if position == 'bas':  # check for luminal or basal boundary
            i = 0  # line of f_term to work on
        else:
            i = -1

        #########################
        # Dirichlet condition
#        if BC_chem[position]['cat'] == 'D':
 #           RHS[i] = f_term[i]

#        else:
        RHS[i] = RHS[i] + step*f_term[i]

    return RHS


def set_f_term_in_RHS(BC_chem):

    f_term = np.zeros(n_mesh_x)

    for position in BC_chem:  # loop over basal and luminal BC

        if position == 'bas':  # check for luminal or basal boundary
            i = 0  # line of f_term to work on

        else:
            i = -1

        ##################
        # Neumann condition
        if BC_chem[position]['cat'] == 'N':
            f = BC_chem[position]['f']
            f_term[i] = diff_coeff*f/step_x

        ##################
        # Robin condition
        elif BC_chem[position]['cat'] == 'R':
            f = BC_chem[position]['f']
            f_term[i] = diff_coeff*f/step_x

        ##################
        # Dirichlet condition
        elif BC_chem[position]['cat'] == 'D':
            f = BC_chem[position]['f']
            f_term[i] = diff_coeff*f/step_x**2

        else:
            raise ValueError('BC cat (%s) is unknown' %
                             BC_chem[position]['cat'])

    return f_term


###############################################################################
# If boundary condition depends of time.

def evaluate_BC_values_at_t(BC_values, chem, t, BC_dep_time, BC=param_c.BC):
    """Modify BC_values with the value of functions for t=t."""

    for position in BC_values[chem]:  # loop over basal and luminal BC
        for j in BC_dep_time[chem][position]:
            BC_values[chem][position][j] = BC[chem][position][j](t)
