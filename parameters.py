#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import numpy as np
from scipy import integrate as scp_integrate

#####################
#### Parameters #####
#####################


sizepop_max = 50000

T_simu = 50.
step_t_cellmov = 0.002
step_t_rd = 5e-5


######################################
# Crypt shape

z_max = 200
r_curv_b = 25.  # radius of curvature at the bottom of the crypt
r_curv_l = 25.  # radius curvature at the luminal extremity of the crypt
z_eps = 0.1*r_curv_b
# Real value: np.sqrt(0.19) = 0.43588989435406733. We set a lower approximation so that
eps = 0.435889
# $\phi does not change sign at the boundary.

######################################
# CELLS

# identification number (int), cell type (int), position(float)
cell_variables = ('id', 'type_c', 'pos')
n_cell_var = len(cell_variables)

cell_size = 10.  # DIAMETER, not radius
k_spring = None  # value is parsed, see end of main.py script.
b_spring = 1.
scaling = None  # parsed


# cell types of the model:pc,gc,ent,dcs
cell_types = ("sc", "pc", "ent", "gc", "dcs")


n_tot_init = 686
n_dcs_init = 12

# Construct a dictionnary which convert cell kind to integer to store the in a numpy array format
cell_to_int = {j: i for i, j in enumerate(cell_types)}
int_to_cell = {i: j for i, j in enumerate(cell_types)}

# Cell types whose position are not fixed.
types_moving = np.array(
    [cell_to_int['sc'], cell_to_int['pc'], cell_to_int['ent'], cell_to_int['gc']])


######################################
# JUMPS
# the jump types of the model (in terms of identity of the new cell, whether it is a differenciation
jump_types = ("sc", "pc", "gc", "ex", "ent")
# or a division)
# Construct a dictionnary which convert jump type to integer to store them in a numpy array format
jump_to_int = {i: x for x, i in enumerate(jump_types)}
int_to_jump = {x: i for x, i in enumerate(jump_types)}


def shift_at_division(z):
    return cell_size/2


def shape_of_crypt(z, radius_curvature_b, radius_curvature_l, z_max):
    """ z is a np.array of size (1,n) of cell position on the longitudinal axis of the crypt.
        Returns a np.array of size (2,n) where the second rows store positions of cell on the radial axis of the crypt.
    """

    radial_position = radius_curvature_b * np.ones(z.size)
    radial_position[z <= radius_curvature_b] = (
        (radius_curvature_b**2 - (z - radius_curvature_b)**2)**0.5)[z <= radius_curvature_b]
    radial_position[z >= z_max - radius_curvature_l] = (-(radius_curvature_l**2 - (
        z - z_max + radius_curvature_l)**2)**0.5 + radius_curvature_b + radius_curvature_l)[z >= z_max - radius_curvature_l]

    return radial_position


# Some computation done once and used for evaluation of the density in eval_density(), in jump_rates.py
step_density = None
correction_table = None


def f_correction_table():

    step_density = cell_size/10
    mesh = np.arange(0, cell_size + step_density, step_density)
    correction_table = np.empty(mesh.shape[0]-1)
    # we discard the last value as it introduces a DivisionByZero error
    for i, j in enumerate(mesh[:-1]):

        # Not a typo in the definition of the lambda function used for integration ! The factor np.exp(1) is at the denominator and
        # numerator and therefore can be canceled.
        correction_table[i] = scp_integrate.quad(
            lambda x: np.exp(-1/(1 - (x/cell_size)**2)), -cell_size, j)[0]

    density_cst_norm = scp_integrate.quad(
        lambda x: np.exp(-1/(1 - (x/cell_size)**2)), -cell_size, cell_size - 1e-13)[0]
    correction_table = density_cst_norm/correction_table

    return correction_table, step_density


integral_D_half_a = scp_integrate.quad(lambda x: np.exp(1 - 1 / (1 - (2*x/cell_size)**2)),
                                       -cell_size/2 + 1e-13, cell_size/2 - 1e-13)[0]

#########################
#### Results storage ####
#########################


pop_hd = {j: i for i, j in enumerate(cell_variables)}  # headers for R_pop

# time, id mother cell, type of mother cell, position of mother cell,
jump_variables = ('t', 'id_mom', 'type_mom', 'pos_mom',
                  'type_jump', 'id_daug', 'pos_daug')
# type of daughter cell, id daughter cell (if applicable)
n_jmp_var = len(jump_variables)
jmp_hd = {j: i for i, j in enumerate(jump_variables)}  # Headers for R_jump


##############################################################
########### Some normalisation constants #####################

def Force(x):
    y = x * np.exp(1 - 1 / (1 - (x/cell_size)))**b_spring
    return y


def argmax_Force(x):
    y = 0.5 * (2 + x - ((2 + x)**2 - 4)**0.5)
    return y


argm_z = cell_size*argmax_Force(b_spring)
C_norm = Force(argm_z)
