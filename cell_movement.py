#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import parameters as param
import numpy as np
import numba
from numba import prange

pop_hd = param.pop_hd


###############################################################################
# SUBFUNCTIONS

@numba.jit(nopython=True)
def func_curvature(position_moving, r_curv_b, r_curv_l, z_max, z_eps, eps):

    Curvature = np.ones(len(position_moving))

    mask1 = position_moving <= r_curv_b - z_eps
    Curvature[mask1] = 1/(1 - eps) * (np.sqrt((position_moving[mask1] + z_eps)/r_curv_b *
                                              (2 - (position_moving[mask1] + z_eps)/r_curv_b)) - eps)
    mask2 = position_moving >= z_max - r_curv_l + z_eps
    Curvature[mask2] = 1/(1 - eps) * (np.sqrt((z_max - position_moving[mask2] + z_eps)/r_curv_b *
                                              (2 - (z_max - position_moving[mask2] + z_eps)/r_curv_b)) - eps)

    return Curvature


@numba.jit(nopython=True)
def support_inter(Xa, n, Xb, m, cell_size):
    # Use the fact that in the context of utilisation, Xa is a vector
    # extracted from Xb.

    out = np.zeros((n, 2), np.int32)
    i_min = 0
    i_max = m - 1

    for i in range(n):

        while Xa[i] - Xb[i_min] > cell_size - 1e-9:
            i_min = i_min + 1

        while Xa[n-1-i] - Xb[i_max] < - cell_size + 1e-9:
            i_max = i_max - 1

        out[i, 0] = i_min
        out[n-1-i, 1] = i_max + 1  # +1 because we want X[i_max] to be included

    return out


@numba.jit(nopython=True)
def force(X):
    """
    Could use numpy, but numba does not seem to recognize numpy func. Therefore,
    we need to use numba.
    """
    Y = X * np.exp(-1 / (1 - np.absolute(X)/param.cell_size) +
                   1) / param.C_norm
    return Y


@numba.jit(nopython=True)  # parallel=True)  # could be parallelized
def sum_force(pos_moving, pos, support, N):
    out = np.zeros(N)

    for i in range(N):  # prange(N):
        out[i] = np.sum(
            force(pos_moving[i] - pos[support[i, 0]:support[i, 1]]))

    return out


@numba.jit(nopython=True)
def force_compute(pos_moving, pos, cell_size, k_spring, scaling):

    N = pos_moving.shape[0]
    M = pos.shape[0]

    support = support_inter(pos_moving, N, pos, M, cell_size)

    F = sum_force(pos_moving, pos, support, N)

    cst = k_spring / scaling
    F = cst * F

    return F


###########################################################
###########################################################
# MAIN FUNCTION


def force_repulsive(position, mask_moving):

    # Parameters
    r_curv_b = param.r_curv_b
    r_curv_l = param.r_curv_l
    z_max = param.z_max
    z_eps = param.z_eps
    eps = param.eps
    cell_size = param.cell_size
    k_spring = param.k_spring
    scaling = param.scaling

    position_moving = position[mask_moving]

    Curvature = func_curvature(
        position_moving, r_curv_b, r_curv_l, z_max, z_eps, eps)

    force = force_compute(position_moving, position,
                          cell_size, k_spring, scaling)

    out = force * Curvature

    return out
