#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np

import parameters as param


chem = ("o2", "buty")
chem_full = {
    "o2": "$O_2$",
    "buty": "butyrate"
}

# Construct a dictionnary which convert chemicals name to integer (will be used for column indexing)
chem_to_int = {j: i for i, j in enumerate(chem)}
int_to_chem = {i: j for i, j in enumerate(chem)}
n_chem = len(chem)


# Space step for diffusion
n_mesh_x = 120
step_x = param.z_max/(n_mesh_x - 1)

if step_x >= param.cell_size:  # must be strictly lower than cell size
    raise ValueError(
        'The spatial mesh size (step_x) must be strictly lower than cell size (cell_size).')

# Add a cell radius at bottom and top of the crypt
# coordinates of mesh points on the x-axis
coord_mesh_x = np.linspace(-param.cell_size/2,
                           param.z_max + param.cell_size/2, n_mesh_x)

# Shape of concentration array that will be used to store chemical concentration
shape_conc = (n_chem, n_mesh_x)

diff_coeff = 3.6e6  # diffusion coefficient (same for all chemicals)


##############################################
#### Boundary conditions for diffusion #######
##############################################


# ******************************************************************************

# Set up a dictionnary for each chemical and each boundary ('bas' = basal, 'lum' =
# luminal):
#    - 'cat': 'D' (Dirichlet), 'N' (Neumann) or 'R' (Robin),
#    - 'f' must be specified for every type of condition,
#    - 'a' must be specified only for Robin condition,
#
# For Dirichlet condition: c = f
#
# For Robin condition: d_z c + a*c = f
#
# For Neumann condition: grad(c) \scal n = f,
# where n is the normal unit vector, pointing
# outward of the domain. Therefore, a positive f will always be a
# positive incoming flux, no matter where on the
# boundary we are situated.
#
# The parameters f,a,b can depend of time.
#
# Then, those dictionaries are stored in a nested dictionnary: BC. Each boundary condition
# call be retrieved as:  BC['buty']['bas'] which denotes boundary condition for
# butyrate at the basal boundary.


# ***************
# ***MUST READ***
# ***************

# If f or a depend of time: in the code below, the variable must be name t.

# ******************************************************************************


# Initialise dictionnary to store all BC:
BC = {}
for i in chem:
    BC[i] = {}
    BC[i]['bas'] = {}
    BC[i]['lum'] = {}


############################
# For butyrate:
buty_BC_bas = {'cat': 'N',
               'f': 0.,
               'a': None
               }
BC['buty']['bas'] = buty_BC_bas


buty_BC_lum = {'cat': 'D',
               'f':5.,# don't define it as a function if it stays constant !
               'a': None
               }

BC['buty']['lum'] = buty_BC_lum


###############################
# For O2:

o2_BC_bas = {'cat': 'D',
             'f': 10,
             'a': None
             }
BC['o2']['bas'] = o2_BC_bas


o2_BC_lum = {'cat': 'N',
             'f': 0.,
             'a': None
             }
BC['o2']['lum'] = o2_BC_lum


################################################################################
################################################################################


#########################
#### Reaction     #######
#########################


# ******************************************************************************

# Stoechio: array storing stoeachiometric coefficient of each reaction.
#
# Max_reaction_speed: array storing the maximal reaction speed of each reaction.
#
# K_reaction: array storing the Michaelis constant of each reaction.
#
# Cataslyst: A nested list storing which cell type catalyse each reaction. Can be None when reaction is not catalyzed by a cell
# but takes place spontaneously in the media.

# ******************************************************************************


# ,'degradation of wnt')
reac = ('respiration of butyrate',)
n_reac = len(reac)
reac_to_int = {j: i for i, j in enumerate(reac)}


# np.array where coefficient a_{i,j} is the
Stoechio = np.zeros((n_reac, n_chem))
# stoechiometric coefficient of chemical j in reaction i. Default coeff are 0.

# array of constant maximal reaction speed
Max_reaction_speed = np.zeros(n_reac)
# for each reaction

# array of Michaelis-Menten constant for each reaction
K_reaction = np.zeros(n_reac)


# A nested list. Each element i is [0] or a list of cell type catalysing
Catalyst = n_reac*[[0]]
# the reaction number i. Make sure that you enter a list or tuple
# even when there is only one cell type. Can be None when reaction is not catalyzed by a cell
# but takes place spontaneously in the media.


###############################################################################
# Respiration of butyrate

# 1 butyrate + 4 o2 -> coproducts

# Set stoechiometric coefficients
Stoechio[reac_to_int['respiration of butyrate'], [
    chem_to_int['buty'], chem_to_int['o2']]] = np.array([-1, -4])  # -4

# Maximal reaction speed
Max_reaction_speed[reac_to_int['respiration of butyrate']
                   ] = 1.6e4
K_reaction[reac_to_int['respiration of butyrate']] = 1840
Catalyst[reac_to_int['respiration of butyrate']] = ('ent', 'gc')


#########################
#### Initialisation #####
#########################


def init_conc():
    DiO = np.linspace(1, 0, n_mesh_x)*o2_BC_bas['f']
    DiB = np.linspace(0, 1, n_mesh_x)*buty_BC_lum['f']

    return DiO, DiB
