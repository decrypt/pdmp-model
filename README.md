# Implementation notice of the piecewise determinisic Markov process (PDMP) model numeric resolution proposed by L. Darrigade and al (2021)

This project propose the resolution of the PDMP individual based model. We propose one Notebooke that will generate figures presented in the article.

Please cite Darrigade et al., 'A PDMP model of the epithelial cell turn-over in the
intestinal crypt including microbiota-derived regulations.' (2021).

## Installation

We assume that virtualenv is installed in your python environment. If it is not the case, execute the command in your shell

```sh
pip install virtualenv
```

To install the packages needed for the code execution, we will install a new python virtual environment

```sh
virtualenv -p python3 JOMB_PDMP
```

Then, activate the virtual environment

```sh
source JOMB_PDMP/bin/activate
```

and install the required packages


```sh
pip install -r requirements.txt
```

You can now run a simulation. To run a simulation, we run the script `main.py`. Simulation results are saved by default at `/simu/simu.npz`. To specify another path, use the `-p` flag as such `python main.py -p YOUR_PATH` (without the `.npz` extension). It can be quite long (3 seconds for 1 hour of simulated time).

```sh
python main.py -p simu/new_simu
```


The jupyter notebook `IBM_notebook` is available to reproduce the figures from the article, on 10 simulation results provided in the `/simu` folder. First, we install a new IPython kernel in our virtual environment that the notebook will use

```sh
ipython kernel install --name JOMB_PDMP --user
```

To open the jupyter notebooks 

```sh
jupyter notebook
```
Then select in the list the jupyter notebook `IBM_notebook`. Once you are in the notebook, make sure the IPython kernel you are using is JOMB_PDMP (if not, go to Kernel > Change kernel and select JOMB_PDMP).



## Git folder composition 

This git folder is composed of 1 Notebook : 
- IBM_notebook : a Notebook to generate figures of the article.


10 python files : 
- main.py : the main Python script to run the PDMP model.
- init.py : for initialisation of the cell population (cell types, numbers and position).
- cell_movement.py : for movement of cells due to mechanical interactions.
- gillepsie.py : for randomly drawing a jump time and type. Call functions from jump_rate.py.
- jump_rates.py : the functions correspondig to the different jump rates, as well as their parameters.
- diffusion.py : for diffusion of chemicals (butyrate and $O_2$ in the current code).
- reaction.py : for reaction of chemicals (consumption of butyrate and $O_2$ by differentiated cells in the current code).
- parameters.py : parameters related to crypt geometry and cells.
- parameters_chem.py : parameters related to chemicals and chemical reactions.
- visualisation.py : module for analysing and plotting results of simulations.


1 pickled files:
- Data_pop_init.p : initial distribution of cells from which the initial position of cells are drawn (used in init.py).

1 folder :
- simu: the default folder where stochastic simulation are saved.

1 compressed folder:
- simu_article.zip : a set of already computed repetitions of the simulations, that can be plotted with the jupyter notebook.

Installation requirements : 
- requirements.txt : the required packages for running simulations (numpy, scipy, numba) and the notebook (matplotlib, notebook).


# General structure of the code

## Parsed argument

At the beginning of main.py, one will see parsed options for simulation. This is to ease toggling with the model. The addition of supplementary parsed arguments can be implemented quite simply, following what is already done in the code.
Note that some parsed arguments have default settings.


## Cell behaviour

Specification of cell types is made in parameters.py. In parameters.py, one must also specify which cell types are fixed (DCS cells for example) and which are mobile.

Enumeration of jump types by name of the jump is done in parameters.py . For example, "sc" is for production of a stem cell, either by symetric division of a stem cell or by differenciation into a stem cell of a cell of a different type. However, all parameters of jump rates are set in jump_intensity.py.



## Diffusion

### Boundary condition
We start from the 3 level nested dictionnary BC defined in parameters_chem.py. First level is for chemical, BC['o2'] for example. Second level is for which boundary (basal side of the crypt or luminal), BC['o2']['bas'] for example. Third level is for coefficients of the boundary condition: 'cat' (Dirichlet, Neumann...), 'f' (a function), 'a' (a function) and 'b' (a function) for a condition:  a * d_z c + b*c = f.
In init_diffusion(), we find which key of the dictionnary BC has a time dependency (that is to say 't' as argument; therefore, it is essential to define time-dependence using 't' as argument).

As we are using implicit scheme for diffusion, the boundary condition is evaluated at t^{n+1}. Therefore, time is updated before treating diffusion.



## Reaction

Parameters of reaction diffusion model are in parameters_chem.py . In parameters_chem.py is done the description of the stoechiometry, the types of cell catalyzing the reaction, etc. 


